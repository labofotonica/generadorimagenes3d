% script_crossed_lines.m

% Este script permite generar las rectas cruzadas en distintos planos
% axiales.

clear, close all;
clc
addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot

%Archivo de salida
folder = 'Output/convolution/2023-02-23_CrossedLines_sigma_3_3_6';
output_filename = 'CrossedLines'; 

%Se genera la PSF
inputs_folder = 'Output/PSF/crossed_lines_tesis';
input_folder = '2023-02-23_12.13.14_PSF_gaussian_sphere_3D_sigma_3_3_6';
%input_folder = '2023-03-16_10.25.45_PSF_gaussian_sphere_3D_sigma_3_3_3';
[params, PSF_work, sigma_work] = load_psf_file(fullfile(inputs_folder, input_folder));

PSF_work = PSF_work/max(PSF_work(:));

sizeAxial = params.sizeAxial;
sizeLateral = params.sizeLateral;
resLateral = params.resLateral; %um
resAxial = params.resAxial; %um

observablePlane = ceil(sizeAxial/2);
observablePlanePSF = ceil(params.sizeAxial/2) + 1;

separacion_z_sigmas = [1 2];

% Particulas
alpha = 100;

imax = 110;

%Ruido y fondo
background_value = 100;
background_mode = 'constant';

imax = imax - background_value;

ruido = true;
%%
if(size(sigma_work, 1) > 1)
   sigma_work = sigma_work(observablePlanePSF, :);
end

%% Generacion de coordenadas para graficos
[~, ~, xPixel, yPixel] = generateGrid([sizeLateral sizeLateral], [1 1], false);
zPixel = linspace(1, sizeAxial, sizeAxial);

%% Plot de la PSF axial.

figure()
plotAxial(PSF_work, [], 'x')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

figure()
plotAxial(PSF_work, [], 'y')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

for sep_sigma = separacion_z_sigmas
    %% Generacion de la entrada.
    % Las funciones generate suman, es decir que es acumulativo el agregado de
    % formas. 
    % Devuelven las posiciones en el rango [1, sz(entrada)].
    entrada = zeros(sizeAxial, sizeLateral, sizeLateral);
    positions = [];

    center = ceil(sizeAxial/2);
    primero = center - floor(sigma_work(3)*sep_sigma/2);
    segundo = center + ceil(sigma_work(3)*sep_sigma/2);

    in = [ceil(33/2) ceil(33/2) 0];
    out = [ceil(sizeLateral/2) ceil(sizeLateral/2) 0];

    [entrada, positions] = generateLine(entrada, positions, 100, [5, 5, primero] - in + out,[29, 29, primero]- in + out, alpha); %3 sigma
    [entrada, positions] = generateLine(entrada, positions, 100, [29, 5, segundo]- in + out,[5, 29, segundo]- in + out, alpha); %3 sigma
    %[entrada, positions] = generateLine(entrada, positions, 100, [12, 12, primero],[20, 20, primero], alpha); %3 sigma
    %[entrada, positions] = generateLine(entrada, positions, 100, [20, 12, segundo],[12, 20, segundo], alpha); %3 sigma
    %%[entrada, positions] = generateLine(entrada, positions, 100, [29, 5, segundo]- in + out,[29, 15, segundo]- in + out, alpha); %3 sigma

    %% Plot ejes laterales con entrada y particulas
    figure()
    plotImageAndParticles(squeeze(sum(entrada(:, :, :),1)), positions(:, 1:2));
    title(['Entrada en plano observable | ' ('size = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'px | sizePixelLat \approx ' + string(resLateral) + ' um')]);

    %%
    figure()
    x = positions(:, 1);
    y = positions(:, 2);
    z = positions(:, 3);
    scatter(x, y, [], z, 'filled')
    xlim([1 sizeLateral])
    ylim([1 sizeLateral])
    caxis([1 sizeAxial])
    colorbar
    axis square
    tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
    title(tit);


    %%
    figure()
    plot3d(positions, [1 sizeLateral 1 sizeLateral 1 sizeAxial], 'b.')
    tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
    title(tit);

    %% Plot ejes axiales con entrada y particulas.
    figure()
    plotImageAndParticlesAxial(entrada, [], 'x', positions, 'sum')
    tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
    title(tit);

    figure()
    plotImageAndParticlesAxial(entrada, [], 'y', positions, 'sum')
    tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
    title(tit);

    %% Convolucion y agregado de ruido y fondo.
    salida = convn(entrada, PSF_work, "same");
    
    %% Normalizo a imax
    salida = salida ./ max(salida(:)) * imax;
    
    %%
    [salida, background_salida] = addBackground(salida, background_value, background_mode);
    if ruido
        [salidaNoise, noise] = addNoise(salida);
        max_cuentas = max(salida(:));
        snr_salida = snr(salida, noise);
        salida = salidaNoise;
    else
       noise = zeros(size(salida)); 
       max_cuentas = max(salida(:))
       snr_salida = snr(salida, noise);
    end
    
    %% Select roi
    % [salida_roi, positions_roi] = getRoi(salida, positions, [size(salida, 2)/2 - 20, size(salida, 2)/2 + 20, size(salida, 3)/2 - 20, size(salida, 3) / 2  + 20, ceil(0.25 * 80), ceil(0.75 * 80)]);
    % salida = salida_roi;
    % positions = positions_roi;

    %% Me quedo con la zona que me interesa
    % centerAxial = ceil(size(salida, 1) / 2);
    % centerLateral = ceil(size(salida, 2) / 2);
    % salida = salida(centerAxial - floor(sizeAxial / 2) + 1 - mod(sizeAxial, 2) : centerAxial + floor(sizeAxial / 2), :, :);
    % salida = salida(:, centerLateral - floor(sizeLateral / 2) + 1 - mod(sizeLateral, 2) : centerLateral + floor(sizeLateral / 2), :);
    % salida = salida(:, :, centerLateral - floor(sizeLateral / 2) + 1 - mod(sizeLateral, 2) : centerLateral + floor(sizeLateral / 2));

    %%
    figure
    obs = ceil(sizeAxial/2);
    imagesc(squeeze(salida(obs, :, :)))
    axis square
    colorbar

    %% Plot en el plano observable.
    figure()
    plotImageAndParticles(squeeze(salida(observablePlane, :, :)), positions(:, 1:2))
    tit = {['Salida en plano observable | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
    title(tit);

    %% Plot 4d convolucion
    figure()
    params_4d.xlabel = 'x [px]';
    params_4d.ylabel = 'y [px]';
    params_4d.zlabel = 'z [px]';
    params_4d.backgroundalpha = 1;
    params_4d.FontSize = 12;
    plot4d(salida, params_4d)
        title(['Convolucion | sigma = ' num2str(sigma_work) ' px'])

    clear params_4d

    %% Plot lateral con salida y particulas.
    figure()
    plotImageAndParticlesAxial(salida, [], 'x', positions, 'sum')
    tit = {['Salida convolucionada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
    title(tit);

    figure()
    plotImageAndParticlesAxial(salida, [], 'y', positions, 'sum')
    tit = {['Salida convolucionada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
    title(tit);
    
    %% Grafico de planos ROI
    center = ceil(size(salida, 1) / 2);
    roi_norm = salida ./ max(salida(:));
    font_size = 12;
    dist_planos = 6;
    for j = center - 2 * dist_planos : dist_planos : center + 2 * dist_planos
        figure
        imagesc([1 size(salida, 3)] * params.resLateral, [1 size(salida, 2)] * params.resLateral, squeeze(roi_norm(j, :, :)))
        xticks([])
        yticks([])
        xlabel('x')
        ylabel('y')
        axis tight equal
        set(gca,'FontSize', font_size,'YDir','normal');
        title(['ROI plano ' num2str(j * params.resAxial * 1e3) ' nm'])
    end
    
    %% Generacion de poblacion inicial
    initial_population = generateInitialPopulation(positions, 100, min(sigma_work(:)) * 3);

    %% Guardado (solo guarda datos del plano observable).
    check_folder(folder);

    ext = '.mat';

    PSF_filename = 'psf';
    roi_filename = 'roi';
    roiPlane_filename = 'roiPlane';
    initial_population_filename = 'initial_population';

    output_filename_ = [output_filename '_snr' num2str(snr_salida, 1) '_back' num2str(background_value, 1) '_sep' num2str(sep_sigma)];

    [folder_name, timestamp] = generate_name_with_time(output_filename_, '3D_vp');
    folder_name = fullfile(folder, folder_name);
    mkdir(folder_name)

    roi_file = generate_name_with_time(roi_filename, output_filename_, timestamp);
    roi_file = fullfile(folder_name, [roi_file ext]);

    roiPlane_file = generate_name_with_time(roiPlane_filename, output_filename_, timestamp);
    roiPlane_file = fullfile(folder_name, [roiPlane_file ext]);

    PSF_file = generate_name_with_time(PSF_filename, output_filename_, timestamp);
    PSF_file = fullfile(folder_name, [PSF_file ext]);



    initial_population_file = generate_name_with_time(initial_population_filename, output_filename, timestamp);
    initial_population_file = fullfile(folder_name, [initial_population_file ext]);

    % Se selecciona como roi solo el plano observable.
    roi = salida;

    zExacto = squeeze(positions(:, 3)).';
    xExacto = squeeze(positions(:, 1)).';
    yExacto = squeeze(positions(:, 2)).';

    % Se guardan solo los datos de sigma y de la PSF en el plano observable.
    PSF = PSF_work;
    roiPlane = squeeze(roi(observablePlane, :, :));

    pxSize = params.resLateral * 1000; %se pasa a nm para SUPPOSe
    pxSizeAxial = params.resAxial * 1000;

    sigma = sigma_work;
    save(roi_file, 'roi', 'positions', 'sigma', 'zExacto', 'xExacto', 'yExacto', 'alpha', 'pxSize', 'pxSizeAxial', 'snr_salida', 'background_salida', 'roiPlane', 'noise', 'max_cuentas');
    save(PSF_file, 'PSF', 'sigma');
    save(initial_population_file, 'initial_population');
end