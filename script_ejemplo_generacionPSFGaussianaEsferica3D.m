% script_ejemplo_generacionPSFGaussianaEsferica3D
%
% Este script de ejemplo genera una PSF gaussiana esferica 3D generada por
% la funcion generateGaussianSphere3D.

clear, close all;
addpath src/psf_generation
addpath src/aux_functions
addpath src/plot

clear, close all;


% Archivo de salida 
folder = 'Output/PSF';
output_filename = 'gaussian_sphere_3D_sigma_3_3_3';

% Parametros
sizeLateral = 31;
sizeAxial = 31;
pxSizeLateral = 0.054; % micrones
pxSizeAxial = 0.035; % micrones
sigma = [3 3];

sample_width = sizeLateral * pxSizeLateral; %micrones
sample_depth = sizeAxial * pxSizeAxial; %micrones

%% Se llama a la funcion de generacion
[PSF, sigma] = generateGaussianSpherePSF3D([sizeAxial sizeLateral sizeLateral], sigma);

%% Plot en eje axial
figure()
plotAxial(PSF, [], 'x')
xlabel('z [px]')
ylabel('x [px]')
title('PSF corte axial')
set(gca, 'FontSize', 12)
figure()
plotAxial(PSF, [], 'y')
xlabel('z [px]')
ylabel('y [px]')
title('PSF corte axial')
set(gca, 'FontSize', 12)

%% Plot en eje lateral
figure()
lateral = linspace(1, sizeLateral, sizeLateral);
plotLateral(squeeze(PSF(ceil(sizeAxial/2), :, :)))
xlabel('x [px]')
ylabel('y [px]')
set(gca, 'FontSize', 12)
title('PSF corte lateral')
axis square
colorbar

%% Guardado
check_folder(folder)
ext = '.mat';
add = 'PSF';

params_conv_filename = 'params_conv';
PSF_filename = 'PSF';

[folder_name, timestamp] = generate_name_with_time(output_filename, add);
folder_name = fullfile(folder, folder_name);
mkdir(folder_name);

params_conv_file = generate_name_with_time(params_conv_filename, output_filename, timestamp);
params_conv_file = fullfile(folder_name, [params_conv_file ext]);

PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
PSF_file = fullfile(folder_name, [PSF_file ext]);

params_conv.sizeLateral = sizeLateral;
params_conv.sizeAxial = sizeAxial;
params_conv.resLateral = sample_width / sizeLateral;
params_conv.resAxial = sample_depth / sizeAxial;
%%
save(params_conv_file, 'params_conv')
save(PSF_file, 'PSF', 'sigma')