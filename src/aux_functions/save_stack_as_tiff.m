function save_stack_as_tiff(stack, file_name)
    stack = stack / 199.8858 * 256;
    imwrite(squeeze(stack(1,:,:)), gray(256), file_name, 'tiff');
    for i = 1 : size(stack, 1)
        imwrite(squeeze(stack(i,:,:)), gray(256), file_name, 'tiff', 'WriteMode', 'append');
    end
end