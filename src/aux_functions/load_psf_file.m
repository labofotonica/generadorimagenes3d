% load_psf_file
%
% Carga archivos de PSF.

function [params_conv, PSF, sigma] = load_psf_file(folder)
    f = fullfile(folder, '*params_conv*');
    listing = dir(f);
    params_conv_filename = fullfile(folder,listing(1).name);
    
    f = fullfile(folder, '*PSF*');
    listing = dir(f);
    PSF_filename = fullfile(folder,listing(1).name);
    
    params_conv = load(params_conv_filename);
    params_conv = params_conv.params_conv;
    PSF = load(PSF_filename);
    sigma = PSF.sigma;
    PSF = PSF.PSF;
    
end