function stack = load_stack_from_tiff(file)
    info = imfinfo(file);
    stacksize = size(info, 1);
    height = info.Height;
    width = info.Width;
    stack = zeros(stacksize, height, width);
    for i = 1 : stacksize
        stack(i, :, :) = uint16(imread(file, i));
    end
end