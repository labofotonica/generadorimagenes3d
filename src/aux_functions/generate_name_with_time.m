% generate_name_with_time
%
% Genera una cadena con los parametros pasados y agrega un timestamp (si se
% pasa uno agrega el que se pasa y sino se genera uno).

function [nombre, time] = generate_name_with_time(nombre, add, timestamp)
    if nargin < 3
        timestamp = datestr(now, 'yyyy-mm-dd_HH.MM.SS');
    end
    nombre = [timestamp '_' add '_' nombre];
    time = timestamp;  
end