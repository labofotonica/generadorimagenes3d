% check_folder
%
% Chequea que exista la carpeta y de no ser asi la crea.

function check_folder(folder)
     if ~exist(folder, 'dir')
       mkdir(folder)
    end
end