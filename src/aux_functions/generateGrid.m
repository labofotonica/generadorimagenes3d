% generateGrid
%
% Devuelve cuatro vectores: dos conteniendo las posiciones x e y, y otros
% dos conteniendo X e Y, siendo estos un grid. Recibe los parametros:
%   *steps = pasos de los vectores, tambien es el tama�o.
%   *resolution = resolucion del vector.
%   *zeroCentered = si es true, el vector comienza en -steps/2 y termina en
%   steps/2. Si es false, comienza en 1 y termina en steps. (Ambos casos
%   multiplicados por resolution).

function [X, Y, x, y] = generateGrid(steps, resolution, zeroCentered)
    if(nargin < 3)
        zeroCentered = false;
    end
    
    if(zeroCentered == true)
        x = linspace(-steps(1) / 2, steps(1)/2, steps(1));
        y = linspace(-steps(2) / 2, steps(2)/2, steps(2));
    else
        x = linspace(1, steps(1), steps(1));
        y = linspace(1, steps(1), steps(1));
    end
    x = x .* resolution(1);
    y = y .* resolution(2);
    
    [X, Y] = meshgrid(x, y);
end