% plotImageAndParticlesAxial
%
% Permite graficar las particulas generadas pasandole el vector con sus
% posiciones sobre el eje axial y otro eje a elegir, todo esto sobre la
% imagen generada.
%
%   *I = vector 3D con la intensidad a graficar.
%   *axial_axis = vector con los valores del eje axial.
%   *lateral_axis = vector con los valores del eje lateral.
%   *positions = vector de tama�o [cantidad de particulas, 3] con las
%   posiciones de las particulas.
%   *axis = eje lateral a graficar:
%       *'x' = se grafica en el eje x.
%       *'y' = se grafica en el eje y.


function plotImageAndParticlesAxial(I, axis_limits, axis, positions, mode)
  if(nargin < 5)
     mode = 'central';
  end
  plotAxial(I, axis_limits, axis, mode)
  plotParticlesAxial(positions, [size(I, 2), size(I, 3)], axis, mode)
  set(gca,'YDir','normal') 
end