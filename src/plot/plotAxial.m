% plotLateralAxial.m
% Se grafica la intensidad de entrada con ejes 'z' y 'x' o 'y'
% segun se elija. El grafico se realiza mirando la linea central
% del eje no graficado.
% plotAxial
% Se grafica la intensidad de entrada con ejes 'z' y 'x' o 'y' segun se
% elija. El grafico se realiza mirando la linea central del eje no
% graficado. Los parameros que recibe:
%
%   *I = vector 3D con la intensidad a graficar.
%   *axis_limits = vector con los limites de los ejes, con formato
%   [axial_limit_0 axial_limit_1 lateral_limit_0 lateral_limit_1], si se
%   ingresa [], se toma por defecto.
%   *axis = eje lateral a graficar:
%       *'x' = se grafica en el eje x.
%       *'y' = se grafica en el eje y.
function plotAxial(I, axis_limits, axis_choose, mode)
    if(nargin < 4)
        mode = 'central';
    end
  
  
    if axis_choose == 'y'
       dim_to_plot = 2;
       dim_to_not_plot = 3;
    elseif axis_choose == 'x'
       dim_to_plot = 3;
       dim_to_not_plot = 2;
    else
        error('Falta eje');
    end
    
    
    I = permute(I, [1, dim_to_plot, dim_to_not_plot]);
    if strcmp(mode, 'central')
        Ieje = squeeze(I(:, :, ceil(size(I, dim_to_not_plot) / 2)));
    elseif strcmp(mode, 'sum')
        Ieje = sum(I, 3);
    else
        error('Modo desconocido')
    end 
    if isempty(axis_limits)
     axis_limits = [1 size(Ieje, 1) 1 size(Ieje, 2)];
    end


    z = linspace(axis_limits(1), axis_limits(2), axis_limits(2));
    x = linspace(axis_limits(3), axis_limits(4), axis_limits(4));
    imagesc(z, x, transpose(Ieje))
    set(gca,'YDir','normal') 
    axis square
    colorbar
    xlim([z(1), z(end)])
    ylim([x(1), x(end)])
end