function plot4d(stack, params)

stack = permute(stack, [1, 3, 2]);% ver bien esto

size_x = size(stack,2);
size_y = size(stack,3);
size_z = size(stack,1);

if(~isfield(params, 'xaxis'))
    params.xaxis = [1 size_x];
end
if(~isfield(params, 'yaxis'))
    params.yaxis = [1 size_y];
end
if(~isfield(params, 'zaxis'))
    params.zaxis = [1 size_z];
end
if(~isfield(params, 'colormap'))
    params.colormap = 'parula';
end
if(~isfield(params, 'backgroundcolor'))
    params.backgroundcolor = [0, 0, 0];
end
if(~isfield(params, 'backgroundalpha'))
    params.backgroundalpha = 1;
end
if(~isfield(params, 'xlabel'))
    params.xlabel = '';
end
if(~isfield(params, 'ylabel'))
    params.ylabel = '';
end
if(~isfield(params, 'zlabel'))
    params.zlabel = '';
end
if(~isfield(params, 'FontSize'))
    params.FontSize = 10;
end

eval(['f = @(size) ' params.colormap '(size);']);
cmap = f(size_z);


% --- Background ---
RI = imref2d([size_x size_y],params.xaxis,params.yaxis);

plano = ones([size_x size_y 3]);
plano(:,:,1) = plano(:,:,1) .* params.backgroundcolor(1);
plano(:,:,2) = plano(:,:,2) .* params.backgroundcolor(2);
plano(:,:,3) = plano(:,:,3) .* params.backgroundcolor(3);
h = imshow(plano,RI,'InitialMagnification','fit');
set(h, 'AlphaData', params.backgroundalpha);

% --- Plot ---
for i = 1 : size_z
    plano = ones([size_x size_y 3]);
    plano(:,:, 1) = plano(:,:, 1) .* cmap(i, 1);
    plano(:, :, 2)  = plano(:,:, 2) .* cmap(i, 2);
    plano(:, :, 3) = plano(:,:, 3) .* cmap(i, 3);
    alpha = squeeze(stack(i, :, :)).' / max(stack(:));
    hold on
    h = imshow(plano, RI, 'InitialMagnification','fit');
    hold off
    set(h, 'AlphaData', alpha)
end

axis on
colormap(gca, cmap);
h = colorbar;
caxis(params.zaxis);

xlim(params.xaxis);
ylim(params.yaxis);


xlabel(params.xlabel)
ylabel(params.ylabel)

set(gca,'FontSize', params.FontSize,'YDir','normal')
h.Label.FontSize = params.FontSize + 2;
h.Label.String = params.zlabel;
h.FontSize = params.FontSize;

end