function plot3d(positions, lims, params)
    tf = ishold;
    if(tf == false)
        hold on
    end
    plot3(positions(: ,1), positions(:, 2), positions(:, 3), params)
    rotate3d on
    
    xlim([lims(1) lims(2)])
    ylim([lims(3) lims(4)])
    zlim([lims(5) lims(6)])
    xlabel('x')
    ylabel('y')
    zlabel('z')
    
    set(gca,'YDir','reverse')
    if(tf == false)
        hold off
    end
    axis square
end