% plotParticles
%
% Permite graficar las particulas generadas pasandole el vector con sus
% posiciones (2D). Recibe los parametros:
%
%   *positions = vector de tama�o [cantidad de particulas 2] con las
%   posiciones de las particulas.

function plotParticles(positions)
    tf = ishold;
    if(tf == false)
        hold on
    end
    for i = 1 : size(positions, 1)
       plot(positions(i, 1), positions(i,2), 'r.', 'LineWidth', 2) 
    end
    set(gca,'YDir','normal') 
    if(tf == false)
        hold off
    end
    axis square
end