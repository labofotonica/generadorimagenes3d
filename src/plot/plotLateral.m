% lo que hace es invertir los ejes para plotear bien en imagesc.
function plotLateral(I, axis_limits)
    if nargin < 2 || isempty(axis_limits)
       axis_limits = [1 size(I, 1) 1 size(I, 2)];
    end
    
    x_axis = linspace(axis_limits(1), axis_limits(2), size(I,1));
    y_axis = linspace(axis_limits(3), axis_limits(4), size(I,2));
    I = I.';
    imagesc(x_axis, y_axis, I);
    xlim([axis_limits(1), axis_limits(2)])
    ylim([axis_limits(3), axis_limits(4)])
end