% plotParticulasAxial
%
% Permite graficar las particulas generadas pasandole el vector con sus
% posiciones sobre el eje axial y otro eje a elegir.
%
%   *positions = vector de tama�o [cantidad de particulas, 3] con las
%   posiciones de las particulas.
%   *sz = vector con el tama�o del vector original donde se ubican las
%   particulas
%   *axis = eje lateral a graficar:
%       *'x' = se grafica en el eje x.
%       *'y' = se grafica en el eje y.

function plotParticlesAxial(positions, sz, axis_choose, mode)
    if(nargin < 4)
       mode = 'central';
    end

    new_positions = [];
    if(strcmp(mode,'central'))
        if(strcmp(axis_choose, 'x'))
            for i = 1 : size(positions, 1)
                if(round(positions(i, 2)) == ceil(sz(2)/2))
                    new_positions = [new_positions; positions(i, 3), positions(i, 1)];
                end
            end
        elseif(strcmp(axis_choose, 'y'))
            for i = 1 : size(positions, 1)
                if(round(positions(i, 1)) == ceil(sz(1)/2))
                    new_positions = [new_positions; positions(i, 3), positions(i, 2)];
                end
            end
        else
            error('Eje incorrecto')
        end
    elseif(strcmp(mode,'sum'))
        if(strcmp(axis_choose, 'x'))
            positions(:, 2) = [];
        elseif(strcmp(axis_choose,'y'))
            positions(:, 1) = [];
        else
            error('Eje incorrecto')
        end
        new_positions = [positions(:, 2), positions(:,1)];
    else
        error('Falta modo')
    end
    
    positions = new_positions;
    clear new_positions;
    
    tf = ishold;
    if(tf == false)
        hold on
    end
    for i = 1 : size(positions, 1)
       plot(positions(i, 1), positions(i,2), 'r.', 'LineWidth', 2) 
    end
    if(tf == false)
        hold off
    end
    axis square
    set(gca,'YDir','normal') 
end