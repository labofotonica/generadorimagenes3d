% plotImageAndParticles
%
% Permite graficar las particulas generadas pasandole el vector con sus
% posiciones (2D) sobre la imagen. Recibe los parametros:
%
%   *x_axis = vector con los valores del eje x.
%   *y_axis = vector con los valores del eje y.
%   *I = vector 2D con la intensidad de la imagen.
%   *positions = vector de tama�o [cantidad de particulas 2] con las
%   posiciones de las particulas.


function plotImageAndParticles(I, positions, axis_limits)
    if(nargin < 3)
        axis_limits = [1 size(I, 1), 1 size(I, 2)];
    end
    x_axis = linspace(axis_limits(1), axis_limits(2), size(I, 1));
    y_axis = linspace(axis_limits(3), axis_limits(4), size(I, 2));
    imagesc(x_axis, y_axis, I)
    plotParticles(positions)
    set(gca,'YDir','normal') 
    colorbar
    
end