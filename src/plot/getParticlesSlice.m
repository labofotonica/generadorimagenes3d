function positions = getParticlesSlice(slice, positions, dim)
    check = (round(positions) == slice);
    check = check(:, dim);
    new_positions = check .* positions;
    limite = size(new_positions, 1);
    i = 1;
    while i < limite
        %si hay uno en 0, igual no se iba a visualizar
        if(new_positions(i, dim) == 0)
            new_positions(i, :) = [];
            i = i - 1;
            limite = limite - 1;
        end
        i = i + 1;
    end

    new_positions(:, dim) = [];
    positions = new_positions;
end
