% convolvePSF
%
% Permite convolucionar una PSF 3D con una muestra 3D. Recibe los
% parametros:
%   *entrada = vector con la muestra 3D de entrada.
%   *PSFvector = vector 4D con la PSF. En la primera coordenada debe ser
%   cada particula, y la segunda debe ser cada plano de su PSF.
%
% IMPORTANTE: La PSF debe ser dada dentro de la muestra, no centrada en la
% posición de su partícula.

function salida = convolvePSF3D(entrada, PSFvector)
    salida = zeros(size(entrada));
    
    for i = 1 : size(entrada, 1)
       pre_entrada = zeros(size(entrada));
       pre_entrada(ceil(size(entrada, 1)/2), :, :) = squeeze(entrada(i, :, :));
       pre_salida = convn(pre_entrada, squeeze(PSFvector(i, :, :, :)), 'same');
       salida = pre_salida + salida;
    end
end
    