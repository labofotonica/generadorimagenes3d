% addBackground
%
% Agrega fondo a la imagen de entrada y devuelve la salida con el fondo
% sumado y el fondo solo. Recibe los parametros:
%
%   *entrada = vector 3D con la entrada.
%   *background_value = valor del fondo o porcentaje del maximo de la
%   entrada, segun modo elegido.
%   *mode = define la manera en que el valor del fondo se define
%       *percentaje = el valor del fondo es el maximo de la entrada
%       multiplicado por background_value.
%       *constant = el valor del fondo es igual a background_value.


function [salida, background] = addBackground(entrada, background_value, mode)
    background = ones(size(entrada));
    if(strcmp(mode, 'factor'))
        background = background * (max(entrada(:)) * background_value);
    elseif(strcmp(mode, 'constant'))
        background = background * background_value;
    else
        error('Opcion no valida');
    end
    
    salida = entrada + background;
end