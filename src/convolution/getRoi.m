function [salida, vector] = getRoi(entrada, vector, points)
    x1 = points(1);
    x2 = points(2);
    y1 = points(3);
    y2 = points(4);
    z1 = points(5);
    z2 = points(6);
    
    salida = entrada(z1:z2, x1:x2, y1:y2);
    
    new_vector = [];
    
    for i = 1 : size(vector, 1)
       if vector(i, 1) >= x1 && vector(i, 1) <= x2 && vector(i, 2) >= y1 && vector(i, 2) <= y2 && vector(i, 3) >= z1 && vector(i, 3) <= z2
           new_vector = [new_vector; vector(i, :)];
       end
    end
    
    vector = new_vector - [x1, y1, z1];
    
end