% addNoise
%
% Agrega ruido poisson a la imagen de entrada y devuelve la salida ruidosa
% y el ruido.


function [salida, ruido] = addNoise(entrada)
    ruido = poissrnd(entrada);
    salida = entrada + ruido;
end 
