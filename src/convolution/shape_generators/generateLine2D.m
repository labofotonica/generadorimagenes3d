% generateLine2D
%
% Permite generar una linea entre los puntos inicio y fin. Devuelve el
% vector de entrada con la nueva linea sumada y las posiciones de las
% nuevas particulas anexadas al vector original. Las posiciones SIEMPRE se
% devuelven entre 0 y sz(vector). Los parametros que recibe:
%
%   *vector = vector de entrada.
%   *posiciones = vector de tama�o [2 cantidad de particulas] con las
%   posiciones de particulas anteriores (vacio si no tiene nada).
%   *inicio = vector de [1 2] con el inicio de la linea.
%   *fin = vector de [1 2] con el final de la linea.
%   *alfa = valor de intensidad de cada particula colocada.
%   *centrado = OPCIONAL. Seteado como true permite colocar inicio y fin
%   centrado en 0, siendo este valor correspondiente a la mitad de la
%   muestra. Esto solo vale en la entrada, la salida siempre se devuelve
%   como se indica anteriormente.

function [vector, posiciones] = generateLine2D(vector, posiciones, particulas, inicio, fin, alfa) 
    pre_vector = zeros([1, size(vector)]);
    pre_vector(1, :, :) = vector;
    pre_posiciones = [];
    [pre_vector, pre_posiciones] = generateLine(pre_vector, pre_posiciones, particulas, [inicio 1], [fin 1], alfa);
    vector = vector + squeeze(pre_vector);
    posiciones = [posiciones pre_posiciones(:, 1:2)];
end