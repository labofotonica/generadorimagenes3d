% generateEllipsoid
%
% Permite generar una elipsoide. Las posiciones SIEMPRE se devuelven entre
% 0 y sz(vector). Los parametros que recibe:
%
%   *vector = vector de entrada.
%   *posiciones = vector de tamaño [3 cantidad de particulas] con las
%   posiciones de particulas anteriores (vacio si no tiene nada).
%   *partorstep = Particulas totales o pasos.
%   *partnotstep = Si es true se usan las particulas totales.
%   *posicion = Posicion del centro de la elipsoide.
%   *radio = radios de la elipsoide.
%   *alfa = valor de intensidad de cada particula colocada.

%https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3223966/
%https://sites.google.com/site/hispeedpackets/Home/antipodallysymmetricpointset?authuser=0

function [vector, posiciones] = generateEllipsoid(vector, posiciones, partorstep, partnotstep, posicion, radio, alfa)
    a = radio(1);
    b = radio(2);
    c = radio(3);

    if (partnotstep)
       K = round(partorstep/2);
    else
       K = round((c / partorstep).^2 * 8 / pi);
    end

    n = round(sqrt(K * pi / 8));
    sc = zeros(K, 2);
    L = pi / sin(pi/4/n);
    sum = 0;
    index = 1;
    for i = 1 : n - 1
        theta = (i - 0.5) * 0.5 * pi / n;
        k = round(2 * pi * sin(theta) * K / L);
        sum = sum + k;

        for j = 1 : k
           phi = (j - 0.5) * 2 * pi / k;
           sc(index, 1) = theta;
           sc(index, 2) = phi;
           index = index + 1;
        end
    end

    theta = (n - 0.5) * 0.5 * pi / n;
    k = K - sum;

    for j = 1 : k 
       phi = (j - 0.5) * 2 * pi / k;
       sc(index, 1) = theta;
       sc(index, 2) = phi;
       index = index + 1;
    end
    
    particulas = zeros(size(sc, 1) * 2, 3);
    theta = [sc(:, 1); -sc(:, 1) + pi];
    phi = [sc(:, 2); sc(:, 2)];
    
    particulas(:, 1) = a * sin(theta(:)) .* cos(phi(:)) + posicion(1);
    particulas(:, 2) = b * sin(theta(:)) .* sin(phi(:)) + posicion(2);
    particulas(:, 3) = c * cos(theta(:)) + posicion(3);
    
    size_new_particles = size(particulas, 1);
    size_posiciones = size(posiciones, 1);
    
    posiciones = [posiciones; particulas];

    
    pre_salida = zeros(size(vector));
    for i = size_posiciones + 1 : size_posiciones + size_new_particles
       pre_salida(round(posiciones(i, 3)), round(posiciones(i, 1)), round(posiciones(i, 2))) = pre_salida(round(posiciones(i, 3)), round(posiciones(i, 1)), round(posiciones(i, 2))) + alfa;
    end
    
    % se permuta para que sea bien visualizado con imagesc.
    vector = vector + permute(pre_salida, [1, 3, 2]);
end