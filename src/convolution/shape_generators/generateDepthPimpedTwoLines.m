% generateDepthPimpedTwoLines
function [vector, posiciones] = generateDepthPimpedTwoLines(vector, posiciones, distancia, largo, inicio_z, fin_z, part_tan, part_ax, alfa)
    if inicio_z > fin_z
      [inicio_z, fin_z] = deal(fin_z, inicio_z);
    end
    dist_ax = (fin_z - inicio_z) / (part_ax - 1);
    for i = inicio_z : dist_ax :  fin_z
       [vector, posiciones] = generatePimpedTwoLines(vector, posiciones, distancia, i, part_tan / 2, largo, alfa);
    end
end