% generateLine
%
% Permite generar una linea entre los puntos inicio y fin. Devuelve el
% vector de entrada con la nueva linea sumada y las posiciones de las
% nuevas particulas anexadas al vector original. Las posiciones SIEMPRE se
% devuelven entre 0 y sz(vector). Los parametros que recibe:
%
%   *vector = vector de entrada.
%   *posiciones = vector de tama�o [3 cantidad de particulas] con las
%   posiciones de particulas anteriores (vacio si no tiene nada).
%   *inicio = vector de [1 3] con el inicio de la linea.
%   *fin = vector de [1 3] con el final de la linea.
%   *alfa = valor de intensidad de cada particula colocada.
%   *centrado = OPCIONAL. Seteado como true permite colocar inicio y fin
%   centrado en 0, siendo este valor correspondiente a la mitad de la
%   muestra. Esto solo vale en la entrada, la salida siempre se devuelve
%   como se indica anteriormente.

function [vector, posiciones] = generateLine(vector, posiciones, particulas, inicio, fin, alfa)
    size_posiciones = size(posiciones, 1);
    x = linspace(inicio(1), fin(1), particulas);
    y = linspace(inicio(2), fin(2), particulas);
    z = linspace(inicio(3), fin(3), particulas);
    posiciones = [posiciones; ([x; y; z].')];
    
    
    pre_salida = zeros(size(vector));
    for i = size_posiciones + 1 : size_posiciones + particulas
       pre_salida(round(posiciones(i, 3)), round(posiciones(i, 1)), round(posiciones(i, 2))) = pre_salida(round(posiciones(i, 3)), round(posiciones(i, 1)), round(posiciones(i, 2))) + alfa;
    end
    
    vector = vector + permute(pre_salida, [1, 3, 2]);
end