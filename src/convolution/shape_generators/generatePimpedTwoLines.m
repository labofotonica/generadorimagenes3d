% generatePimpedTwoLines
%
% Genera dos lineas inclinadas 45 grados, separados por una distancia
% ortogonal. Se le pasan los parametros:

%   *vector = vector de entrada.
%   *posiciones = vector de tama�o [3 cantidad de particulas] con las
%   posiciones de particulas anteriores (vacio si no tiene nada).
%   *distancia = distancia de separacion en pixeles.
%   *plano_z = plano donde se ubican las lineas.
%   *particulas = cantidad de particulas de cada linea.
%   *alfa = valor de intensidad de cada particula colocada.
%   *centrado = OPCIONAL. Seteado como true permite colocar plano_z
%   centrado en 0, siendo este valor correspondiente a la mitad de la
%   muestra. Esto solo vale en la entrada, la salida siempre se devuelve
%   como se indica anteriormente.

function [vector, posiciones] = generatePimpedTwoLines(vector, posiciones, distancia, plano_z, particulas, largo, alfa)

    center = [size(vector, 2) size(vector, 3)] / 2;
    centro_abajo = center + [distancia / sqrt(8), - distancia / sqrt(8)];
    centro_arriba = center - [distancia / sqrt(8), - distancia / sqrt(8)];
    
    inicio_abajo = centro_abajo - [largo / sqrt(8), largo/sqrt(8)];
    fin_abajo = centro_abajo + [largo / sqrt(8), largo/sqrt(8)];
    inicio_arriba = centro_arriba - [largo / sqrt(8), largo/sqrt(8)];
    fin_arriba = centro_arriba + [largo / sqrt(8), largo/sqrt(8)];
    
    inicio_abajo = ceil(inicio_abajo);
    fin_abajo = ceil(fin_abajo);
    inicio_arriba = ceil(inicio_arriba);
    fin_arriba = ceil(fin_arriba);
    
    
    [vector, posiciones] = generateLine(vector, posiciones, particulas, [inicio_abajo plano_z], [fin_abajo, plano_z], alfa);
    [vector, posiciones] = generateLine(vector, posiciones, particulas, [inicio_arriba, plano_z], [fin_arriba, plano_z], alfa);
    
    
end
