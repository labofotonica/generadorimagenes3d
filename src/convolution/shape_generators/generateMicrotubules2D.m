% generateMicrotubules2D
%
% Permite generar microtubulos sintenticos. Devuelve el
% vector de entrada con las figuras sumadas y las posiciones de las
% nuevas particulas anexadas al vector original. Las posiciones SIEMPRE se
% devuelven entre 0 y sz(vector). Los parametros que recibe:
%
%   *vector = vector de entrada.
%   *posiciones = vector de tamaño [3 cantidad de particulas] con las
%   posiciones de particulas anteriores (vacio si no tiene nada).
%   *n_microtubules = cantidad de microtubulos generados.
%   *alfa = valor de intensidad de cada particula colocada.
%   *params = struct con parametros para la configuración del generador.
%    Ver la documentación de Tubulin2D.

function [vector, posiciones, cant_particulas] = generateMicrotubules2D(vector, posiciones, n_microtubules, alfa, params)
    
    size_posiciones = size(posiciones, 1);
    size_vector = size(vector); %yx
    
    
    
    new_posiciones = Tubulin2D(size_vector, 1, n_microtubules, params);
    new_posiciones = new_posiciones+1;
    x = new_posiciones(:, 2);
    y = new_posiciones(:, 1);
    cant_particulas = size(new_posiciones, 1);
    posiciones = [posiciones; ([x y])];
    
    pre_salida = zeros(size(vector));

    for i = size_posiciones + 1 : size_posiciones + cant_particulas
       pre_salida(round(posiciones(i, 1)), round(posiciones(i, 2))) = pre_salida(round(posiciones(i, 1)), round(posiciones(i, 2))) + alfa;
    end
    
    vector = vector + permute(pre_salida, [2,1]);
    
    
end