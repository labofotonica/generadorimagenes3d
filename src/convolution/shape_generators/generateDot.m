% generateDot
%
% Permite generar un punto en el punto posicion. Devuelve el vector de
% entrada con el nuevo punto sumado y las posiciones de las nuevas
% particulas anexadas al vector original. El punto se genera generando una
% "nube" de puntos con distribucion gaussiana o uniforme. Los parametros
% que recibe:
%
%   *vector = vector de entrada.
%   *posiciones = vector de tama�o [3 cantidad de particulas] con las
%   posiciones de particulas anteriores (vacio si no tiene nada).
%   *posicion = vector de [1 3] con la posicion.
%   *radio = radio del punto. En caso de ser gaussiana sigma = radio / 3. En
%   caso de configurarse un solo punto, esto es ignorado (igualmente el
%   valor debe estar).
%   *alfa = valor de intensidad de cada particula colocada.
%   *distribucion = 
%       *'gaussian' = distribucion gaussiana de los puntos alrededor de la
%       posicion y con sigma = radio * 3.
%       *'uniform' = distribucion uniforme de los puntos alrededor de la
%       posicion y del radio indicado.
%       *'onlydot' = todos las particulas se colocan en un solo punto
%       indicado por la posicion.
%   *centrado = OPCIONAL. Seteado como true permite colocar inicio y fin
%   centrado en 0, siendo este valor correspondiente a la mitad de la
%   muestra. Esto solo vale en la entrada, la salida siempre se devuelve
%   como se indica anteriormente.



function [vector, posiciones] = generateDot(vector, posiciones, particulas, posicion, radio, alfa, distribucion)
   
    size_posiciones = size(posiciones, 1);
    if(strcmp(distribucion, 'gaussian'))
        sigma = radio/3;
        posiciones = [posiciones; randn(particulas, 3) .* sigma + posicion];
    elseif (strcmp(distribucion, 'uniform'))
         i = 1;
        posiciones = [posiciones; zeros(particulas, 3)];
        while(i <= particulas)
            candidata = (rand(1, 3) - 0.5) .* radio * 2 + posicion;
            if(norm(candidata - posicion)^2 < min(radio)^2)
                posiciones(size_posiciones + i, :) = candidata;
                i = i + 1;
            end
        end
    elseif (strcmp(distribucion, 'onlydot'))
        posiciones = [posiciones; ones(particulas,3) .* posicion];
    else
        error('Distribuci�n invalida');
    end
    pre_salida = zeros(size(vector));
    for i = size_posiciones + 1 : size_posiciones + particulas
       pre_salida(round(posiciones(i, 3)), round(posiciones(i, 1)), round(posiciones(i, 2))) = pre_salida(round(posiciones(i, 3)), round(posiciones(i, 1)), round(posiciones(i, 2))) + alfa;
    end
    
    % se permuta para que sea bien visualizado con imagesc.
    vector = vector + permute(pre_salida, [1, 3, 2]);
end