% Tubulin2D
%
% Permite utilizar desde MATLAB la función make_n_objects() de la clase de
% la clase Tubulin2D del paquete samples.py. Para ello se guardan los
% parametros en un archivo llamado 'config.mat' para ser pasados al script
% Tubulin2D.py. Este ejecuta el método y guarda los resultados en un
% archivo 'output.py'. Esta función toma las posiciones de particulas
% resultantes, las ordena (recorta si es necesario) y las devuelve.
%
%   image_shape = numero o vector de largo 2 con [size_y, size_x] que
%   indica el tamaño de la imagen.
%   *pixel_size = numero o vector de larog 2 con [pixel_size_y,
%   pixel_size_x] que indica tamaño del pixel (junto con el parametro
%   sampling factor de params, define que tantas particulas se colocan por
%   'pixel').
%   *nObjects = cantidad de microtubulos generados.
%   *params = struct con parametros para la configuración del generador.
%    Ver la documentación de Tubulin2D.

function positions = Tubulin2D(image_shape, pixel_size, nObjects, params)
    % Verificar lo de zyx o xyz.
    if(nargin < 4)
        params = struct([]);
    end
    if(nargin < 3)
        error('Not enough input arguments')
    end
    
    error_signal__ = 'error';
    
    
    if length(image_shape) == 2
        image_shape = [image_shape(1), image_shape(2)];
    elseif length(image_shape) == 1
        image_shape = [image_shape image_shape];
    else
        error('image_shape wrong length')
    end
    
    if length(pixel_size) == 2
        pixel_size = [pixel_size(1), pixel_size(2)];
    elseif length(pixel_size) == 1
        pixel_size = [pixel_size pixel_size];
    else
        error('pixel_size wrong length')
    end
       
    [filepath,~,~] = fileparts(mfilename('fullpath'));
    python_path = fullfile(filepath, 'python');
    tubulin_path = fullfile(python_path, 'Tubulin2D.py');
    tubulin_path = ['"' tubulin_path '"'];
    config_path = fullfile(python_path, 'config.mat');
    output_path = fullfile(python_path, 'output.mat');
    
    
    
    params_fieldnames = fieldnames(params);
    params = struct2cell(params);
    
    save(config_path, 'error_signal__', 'image_shape', 'pixel_size', 'nObjects', 'params', 'params_fieldnames')
    
    
    
    [~,result] = system(['python ' tubulin_path]);
    
    if(~isempty(result))
        error(['Something went wrong with python:' newline result])
    end
    
    positions = load(output_path);
    positions = positions.output;
    positions = [positions(:,2), positions(:,1)];
    positions = positions + image_shape/2;
  
    
%     scatter(positions(:,1),positions(:,2))
%     hold on
    
    positions = positions(min(positions > 0, [], 2), :);
    positions = positions(min(positions < (image_shape-1), [], 2), :);

%     scatter(positions(:,1),positions(:,2))
%     
%     xlabel('x')
%     ylabel('y')
%     figure
%     scatter(positions(:,1),positions(:,2))
%     
%     xlabel('x')
%     ylabel('y')
    
end