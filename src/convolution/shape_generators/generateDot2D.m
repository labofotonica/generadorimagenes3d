% generateDot2D
%
% Permite generar un punto en el punto posicion. Devuelve el vector de
% entrada con el nuevo punto sumado y las posiciones de las nuevas
% particulas anexadas al vector original. El punto se genera generando una
% "nube" de puntos con distribucion gaussiana o uniforme. Los parametros
% que recibe:
%
%   *vector = vector de entrada.
%   *posiciones = vector de tama�o [2 cantidad de particulas] con las
%   posiciones de particulas anteriores (vacio si no tiene nada).
%   *posicion = vector de [1 2] con la posicion.
%   *radio = radio del punto. En caso de ser gaussiana sigma = radio / 3. En
%   caso de configurarse un solo punto, esto es ignorado (igualmente el
%   valor debe estar).
%   *alfa = valor de intensidad de cada particula colocada.
%   *distribucion = 
%       *'gaussian' = distribucion gaussiana de los puntos alrededor de la
%       posicion y con sigma = radio * 3.
%       *'uniform' = distribucion uniforme de los puntos alrededor de la
%       posicion y del radio indicado.
%       *'onlydot' = todos las particulas se colocan en un solo punto
%       indicado por la posicion.
%   *centrado = OPCIONAL. Seteado como true permite colocar inicio y fin
%   centrado en 0, siendo este valor correspondiente a la mitad de la
%   muestra. Esto solo vale en la entrada, la salida siempre se devuelve
%   como se indica anteriormente.

function [vector, posiciones] = generateDot2D(vector, posiciones, particulas, posicion, radio, alfa, distribucion)
    pre_vector = zeros([1, size(vector)]);
    pre_vector(1, :, :) = vector;
    pre_posiciones = [];
    [pre_vector, pre_posiciones] = generateDot(pre_vector, pre_posiciones, particulas, [posicion 1], [radio radio 0], alfa, distribucion);
    vector = vector + squeeze(pre_vector);
    posiciones = [posiciones pre_posiciones(:, 1:2)];
end