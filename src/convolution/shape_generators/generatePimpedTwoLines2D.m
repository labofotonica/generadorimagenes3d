% generatePimpedTwoLines2D
%
% Genera dos lineas inclinadas 45 grados, separados por una distancia
% ortogonal. Se le pasan los parametros:

%   *vector = vector de entrada.
%   *posiciones = vector de tama�o [2 cantidad de particulas] con las
%   posiciones de particulas anteriores (vacio si no tiene nada).
%   *distancia = distancia de separacion en pixeles.
%   *particulas = cantidad de particulas de cada linea.
%   *alfa = valor de intensidad de cada particula colocada.

function [vector, posiciones] = generatePimpedTwoLines2D(vector, posiciones, distancia, particulas, alfa)
    pre_vector = zeros([1, size(vector)]);
    pre_vector(1, :, :) = vector;
    pre_posiciones = [];
    [pre_vector, pre_posiciones] = generatePimpedTwoLines(pre_vector, pre_posiciones, distancia, 1, particulas, alfa);
    vector = vector + squeeze(pre_vector);
    posiciones = [posiciones pre_posiciones(:, 1:2)];
end
