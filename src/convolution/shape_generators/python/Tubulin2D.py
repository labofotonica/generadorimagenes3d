from pysuppose_tools.samples import Tubulin2D
from scipy import io
import numpy as np
import os

"""
    Este script permite ejecutar desde matlab la función make_n_objects() de la clase Tubulin2D del paquete samples.py.
    Para ello el script hace un parseo de los parametros pasados desde matlab a través del archivo 'config.mat'. Luego ejecuta
    el método y guarda los resultados en el archivo 'output.mat'.
"""


abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)


config = io.loadmat('config.mat')

if '__header__' in config:
    del config['__header__']
if '__version__' in config:
    del config['__version__']
if '__globals__' in config:
    del config['__globals__']
if 'error_signal__' in config:
    error_signal = config['error_signal__']
    del config['error_signal__']


if 'image_shape' in config:
    image_shape = config['image_shape']
    image_shape = image_shape[0]
    del config['image_shape']
else:
    print(error_signal)
    exit()
if 'pixel_size' in config:
    pixel_size = config['pixel_size']
    pixel_size = pixel_size[0]
    del config['pixel_size']
else:
    print(error_signal)
    exit()
if 'nObjects' in config:
    nObjects = config['nObjects']
    nObjects = nObjects[0, 0]
    del config['nObjects']
else:
    print(error_signal)
    exit()


generator = Tubulin2D(image_shape, pixel_size)

if 'params' in config:
    params_prev = config['params']
    del config['params']
else:
    print(error_signal)
    exit()
if 'params_fieldnames' in config:
    params_fieldnames = config['params_fieldnames']
    del config['params_fieldnames']
else:
    print(error_signal)
    exit()

params = {}
for i in range(0,len(params_fieldnames)):
    fieldname = params_fieldnames[i][0][0]
    lista = []
    for j in range(0, len(params_prev[i][0][0])):
        element = params_prev[i][0][0][j]
        while type(element) is list or type(element) is np.ndarray:
            element = element[0]
        lista.append(element)
    params[fieldname] = tuple(lista)
        

if 'sampling_factor' in params:
    sampling_factor = params['sampling_factor']
    sampling_factor = sampling_factor[0]
    generator.sampling_factor = sampling_factor.astype(int)
    del params['sampling_factor']

if 'tube_length' in params:
    generator.tube_length = params['tube_length']
    del params['tube_length']

if 'random_longitudinal' in params:
    random_longitudinal = params['random_longitudinal']
    random_longitudinal = random_longitudinal[0]
    generator.random_longitudinal = random_longitudinal
    del params['random_longitudinal']

if 'random_transverse' in params:
    random_transverse = params['random_trasnverse']
    random_transverse = random_transverse[0]
    generator.random_transverse = random_transverse
    del params['random_transverse']

if 'kmax' in params:
    kmax = params['kmax']
    kmax = kmax[0]
    generator.kmax = kmax
    del params['kmax']

if 'sigma_dk' in params:
    sigma_dk = params['sigma_dk']
    sigma_dk = sigma_dk[0]
    generator.sigma_dk = sigma_dk
    del params['sigma_dk']

if 'tmax' in params:
    tmax = params['tmax']
    tmax = tmax[0]
    generator.tmax = tmax
    del params['tmax']

if 'sigma_dt' in params:
    sigma_dt = params['sigma_dt']
    sigma_dt = sigma_dt[0]
    generator.sigma_dt = sigma_dt
    del params['sigma_dt']

if 'rotate' in params:
    rotate = params['rotate']
    rotate = rotate[0]
    generator.rotate = rotate
    del params['rotate']

if 'translate' in params:
    translate = params['translate']
    translate = translate[0]
    generator.translate = translate
    del params['translate']

if 'allow_empty_images' in params:
    allow_empty_images = params['allow_empty_images']
    allow_empty_images = allow_empty_images[0]
    generator.allow_empty_images = allow_empty_images
    del params['allow_empty_images']


if params:
    print(error_signal)
    exit()


generation = generator.make_n_objects(nObjects)

io.savemat('output.mat', {'output': generation})
