% generateMicrotubules3D
%
% Permite generar microtubulos sintenticos. Devuelve el
% vector de entrada con las figuras sumadas y las posiciones de las
% nuevas particulas anexadas al vector original. Las posiciones SIEMPRE se
% devuelven entre 0 y sz(vector). Los parametros que recibe:
%
%   *vector = vector de entrada.
%   *posiciones = vector de tamaño [3 cantidad de particulas] con las
%   posiciones de particulas anteriores (vacio si no tiene nada).
%   *n_microtubules = cantidad de microtubulos generados.
%   *alfa = valor de intensidad de cada particula colocada.
%   *params = struct con parametros para la configuración del generador.
%    Ver la documentación de Tubulin3D.


function [vector, posiciones, cant_particulas] = generateMicrotubules3D(vector, posiciones, n_microtubules, alfa, params)
    
    size_posiciones = size(posiciones, 1);
    size_vector = size(vector); %zyx
    
    new_posiciones = Tubulin3D(size_vector, 1, n_microtubules, params);
    new_posiciones = new_posiciones+1;
    x = new_posiciones(:, 3);
    y = new_posiciones(:, 2);
    z = new_posiciones(:, 1);
    cant_particulas = size(new_posiciones, 1);
    posiciones = [posiciones; ([x y z])];
    
    pre_salida = zeros(size(vector));

    for i = size_posiciones + 1 : size_posiciones + cant_particulas
       pre_salida(round(posiciones(i, 3)), round(posiciones(i, 1)), round(posiciones(i, 2))) = pre_salida(round(posiciones(i, 3)), round(posiciones(i, 1)), round(posiciones(i, 2))) + alfa;
    end
    
    vector = vector + permute(pre_salida, [1,3,2]);
    
    
end