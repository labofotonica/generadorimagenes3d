function population = generateInitialPopulation(positions, sz, kick_size) 
    population = zeros(sz, size(positions, 1), size(positions, 2));
    for i = 1 : sz
        population(i, :, :) = positions + randn(size(positions)) * kick_size;
    end
end