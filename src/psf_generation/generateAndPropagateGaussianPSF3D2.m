% generateAndPropagateGaussianPSF3D
%
% Funci�n que genera una gaussiana 3D propagada en el sistema dado por las
% funciones matrix_x y matrix_y. Comenzando siempre por un microscopio con
% aumento M.
% Se genera una matriz 4D de salida, donde la primera dimensi�n es cada
% particula, la segunda es cada posicion de la PSF y las ultimas dos x e
% y. Recibe los parametros:
%
%   *sz: tama�o de la PSF 3D.
%   *params_PSF: struct con parametros de la gaussiana, que debe
%   contener:
%       *sample_depth = Profundidad de la muestra.
%       *lambda = Largo de onda de la emision.
%       *w0 = cintura original de la emision.
%       *sample_width = ancho y alto de la PSF en micrones.
%   *params_propagation: struct con parametros de la propagacion para
%   configuracion del sistema de matrices. Los parametros que debe tener son:
%       *camera_delta = diferencia en la posicion central de la camara respecto
%       al foco del sistema de propagacion.
%       *nm = indice de refraccion de la muestra.
%       *M = magnificacion del microscopio
%       *m = magnificacion del sistema (excluyendo microscopio).
%   *xMatrixFunction, yMatrixFunction = funciones de generacion de las
%   matrices de propagacion. De no pasarse alguna de las funciones, no se
%   propagara. (La matriz ser� [1 0; 0 1]).


% sz = [z, x, y]
function [ salida, sigma ] = generateAndPropagateGaussianPSF3D2(sz, params_PSF, params_propagation, xMatrixFunction, yMatrixFunction)
    matrix_function = true;
    if nargin < 5
       matrix_function = false; 
    end


    if ~isfield(params_PSF, 'sample_depth')
        error('Falta el parametro sample_depth')
    end
    if ~isfield(params_PSF, 'lambda')
        error('Falta el parametro lambda')
    end
    if ~isfield(params_PSF, 'w0')
        error('Falta el parametro w0')
    end
    if ~isfield(params_PSF, 'sample_width')
        error('Falta el parametro sample_width')
    end
    if ~isfield(params_PSF, 'nm')
        error('Falta el parametro nm')
    end
    if ~isfield(params_PSF, 'pZ')
        error('Falta el parametro pZ')
    end
    
    if matrix_function == true
        if ~isfield(params_propagation, 'camera_delta')
            error('Falta el parametro camera_delta')
        end
        if ~isfield(params_propagation, 'M')
            error('Falta el parametro M')
        end
        if ~isfield(params_propagation, 'm')
            error('Falta el parametro m')
        end
    end
    sz_z_ = sz(1);
    sz_z = ceil(sz_z_ * 1.5);
    sz_x = sz(2);
    sz_y = sz(3);
    
    steps = linspace(0, params_PSF.sample_depth, sz_z); %pasos y posiciones de particulas
    params_propagation.pZ = params_PSF.pZ;
    
    sigma = zeros([sz_z, 3]);
    
    salida = zeros([sz_z, sz_x, sz_y]);
    
    [X, Y] = generateGrid([sz_x, sz_y], [params_PSF.sample_width params_PSF.sample_width] ./ [sz_x, sz_y], true);
    
    
    zr = params_PSF.w0^2 * pi / params_PSF.lambda;
    
    if matrix_function == true
        fixed_camera_delta = params_propagation.camera_delta;
        camera_delta = linspace(-params_PSF.sample_depth, params_PSF.sample_depth, sz_z);
        camera_delta = camera_delta * (params_propagation.M * params_propagation.m).^2 / params_PSF.nm;
        camera_delta = camera_delta + fixed_camera_delta;
        camera_delta = flip(camera_delta);
    else
       camera_delta = zeros(sz_z); 
    end
    % El principio de la muestra es 0. Se hace la propagacion plano por
    % plano.
    for j = 1 : sz_z %Se itera por los diferentes planos

       q = (params_propagation.pZ - steps(j)) / params_PSF.nm + 1i * zr;
       params_propagation.camera_delta = camera_delta(j);
       if matrix_function == true
        matrix_x = xMatrixFunction(2, params_propagation) * xMatrixFunction(1, params_propagation);
        matrix_y = yMatrixFunction(2, params_propagation) * yMatrixFunction(1, params_propagation);
       else
        matrix_x = [1 0; 0 1];
        matrix_y = [1 0; 0 1];
       end
       

       q2_x = (matrix_x(1) * q + matrix_x(3)) / (matrix_x(2) * q + matrix_x(4));
       q2_y = (matrix_y(1) * q + matrix_y(3)) / (matrix_y(2) * q + matrix_y(4));

       q2 = [q2_x q2_y];
       zr2 = imag(q2);

       w02 = sqrt(zr2 * params_PSF.lambda / pi);
       w2 = w02 .* sqrt(1 + (real(q2)./zr2).^2);

       sigma(j, 1:2) = w2 .* ([sz(2) sz(3)] - [1 1]) ./ [params_PSF.sample_width params_PSF.sample_width] / 2;
       alpha = (sqrt(exp(1/2)-1));
       sigma(j, 3) = alpha * zr * sz_z / params_PSF.sample_depth;
       salida(j, :, :) = (w02(1)./w2(1)) .* (w02(2)./w2(2)) .* exp(-2*((X./w2(1)).^2 + (Y./w2(2)).^2));
    end
   
    [~, center_index] = min(abs(sigma(:, 1) - sigma(:,2)));
    salida = salida(center_index - floor(sz_z_/2) : center_index + floor(sz_z_/2) + 1, :, :);
    sigma = sigma(center_index - floor(sz_z_/2): center_index + floor(sz_z_/2) + 1, :);
end