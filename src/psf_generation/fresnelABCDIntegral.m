% fresnelABCDIntegral
%
% Realiza la integral de fresnel para propagar a traves de sistemas ABCD.
% Recibe los paraemtros:
%   *E = vector 2D con campo electrico complejo.
%   *axis = estructura con los campos:
%       *x_start e y_start = vector con los valores de las coordenadas
%       anteriores a la propagacion.
%       *x e y = vector con los valores de las coordenadas posteriores a la
%       propagacion.
%       *lambda = longitud de onda en micrones.
%       *mat_x y mat_y = matrices ABCD con las que se propaga en x y en y
%       respectivamente.

function E_parcial = fresnelABCDIntegral(E, axis, lambda, mat_x, mat_y)
    
    x_start = axis.x_start;
    y_start = axis.y_start;
    x = axis.x;
    y = axis.y;
    
    [XInicio, YInicio] = meshgrid(x_start, y_start);

    aX = mat_x(1);
    bX = mat_x(3);
    dX = mat_x(4);
    aY = mat_y(1);
    bY = mat_y(3);
    dY = mat_y(4);
    E_parcial = zeros(size(x, 2), size(y, 2));

    %Integracion
    for i = 1 : size(x, 2)
        for j = 1 : size(y, 2)
            E_parcial(i, j) = trapz(y_start, trapz(x_start, E .* exp(1i * 2* pi - 1i * pi /(bX * lambda) * (aX * XInicio.^2 - 2 * x(1, i) * XInicio + dX * x(1, i).^2)) .* exp(1i * 2*pi - 1i * pi /(bY * lambda) *(aY * YInicio.^2 - 2 * y(1,j) * YInicio + dY * y(1,j).^2)), 2));
        end
    end

end