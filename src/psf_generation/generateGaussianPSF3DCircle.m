% generateGaussianPSF3D
%
% Se genera una PSF gaussiana 3D constante.
% Solo w0 igual en x e y.


% sz = [z, x, y]
function [ salida, sigma ] = generateGaussianPSF3DCircle(sz, params_PSF)
    if ~isfield(params_PSF, 'sample_depth')
        error('Falta el parametro sample_depth')
    end
    if ~isfield(params_PSF, 'lambda')
        error('Falta el parametro lambda')
    end
    if ~isfield(params_PSF, 'w0')
        error('Falta el parametro w0')
    end
    if ~isfield(params_PSF, 'sample_width')
        error('Falta el parametro sample_width')
    end
    if ~isfield(params_PSF, 'nm')
        error('Falta el parametro nm')
    end
    if ~isfield(params_PSF, 'z')
        error('Falta el parametro z')
    end
    
    sz_z = sz(1);
    sz_x = sz(2);
    sz_y = sz(3);
    
    steps = linspace(0, params_PSF.sample_depth, sz_z); %pasos y posiciones de particulas
    z = params_PSF.z;
    
    sigma_raw = zeros([sz_z, 2]);
    
    salida = zeros([sz_z, sz_x, sz_y]);
    
    [X, Y] = generateGrid([sz_x, sz_y], [params_PSF.sample_width params_PSF.sample_width] ./ [sz_x, sz_y], true);
    
    
    zr = params_PSF.w0^2 * pi / params_PSF.lambda;
    
    % El principio de la muestra es 0. Se hace la propagacion plano por
    % plano.
    for j = 1 : sz_z %Se itera por los diferentes planos

       q = (z - steps(j)) / params_PSF.nm + 1i * zr;

       q2 = [q q];
       zr2 = imag(q2);

       w02 = sqrt(zr2 * params_PSF.lambda / pi);
       w2 = w02 .* sqrt(1 + (real(q2)./zr2).^2);

       sigma_raw(j, :) = w2 .* ([sz(2) sz(3)] - [1 1]) ./ [params_PSF.sample_width params_PSF.sample_width] / 2;
       %salida(j, :, :) = (w02(1)./w2(1)) .* (w02(2)./w2(2)) .* exp(-2*((X./w2(1)).^2 + (Y./w2(2)).^2));
       salida(j, :, :) = (w02(1)./w2(1)) .* (w02(2)./w2(2)) .* ((X.^2 / sigma_raw(j, 1).^2) + (Y.^2 / sigma_raw(j, 2).^2) < 1);
    end
    
    % se da un sigma para x, otro para y (en 0) y otro para z.
    sigma_lateral = min(sigma_raw(:, 1));
    alpha = (sqrt(exp(1/2)-1));
    sigma_z = alpha * zr * sz(1) / params_PSF.sample_depth;
    sigma = [sigma_lateral sigma_lateral sigma_z];
end