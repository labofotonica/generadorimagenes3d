% max 3D. supone que, si bien no son funciones similares a gaussianas, son
% decrecientes desde el maximo hacia los lados y con simetria (o al menos
% algo similar a simetria. method puede ser 'intensity' o 'fit'.

function sigma = sigma_estimation(I, method)
    if(nargin < 2)
       method = 'intensity'; 
    end

    [maximum, idx] = max(I(:));
    I = I / maximum;
    sz = size(I);
    dots = 1000;

    [x, y, z] = ind2sub(sz, idx);
    centre = [x y z];
    sigma = zeros(1, 3);
    
    if strcmp(method, 'intensity')
        for i = 1 : 3
            centre = circshift(centre, 1);
            I = permute(I, [3, 1, 2]);

            I_eje = squeeze(I(:, centre(2), centre(3)));
            [~, ind] = min(abs(I_eje(:) - exp(-1/2)));
            minimum = I_eje(ind);
            if (ind ~= 1) && (ind ~= sz(1))
                %aprox lineal
                if (minimum > exp(-1/2) && ind < centre(1)) || (minimum < exp(-1/2) && ind > centre(1))
                   ind = ind - 1;   
                end
                extension = linspace(I_eje(ind), I_eje(ind + 1), dots);
                [~, ind2] = min(abs(extension(:) - exp(-1/2)));
                ind = ind + ind2 / dots;
            end
            sigma(i) = abs(ind - centre(1));
        end
    elseif strcmp(method, 'fit')
        for i = 1 : 3
            centre = circshift(centre, 1);
            sz = circshift(sz, 1);
            I = permute(I, [3, 1, 2]);

            I_eje = squeeze(I(:, centre(2), centre(3)));
            x = linspace(1, sz(1), sz(1)); % ver esto
            f = fit(x.',I_eje,'gauss1');
            sigma(i) = f.c1 / sqrt(2);
        end
    else
        error('Metodo incorrecto');
    end
    sigma = [sigma(2), sigma(1), sigma(3)];
end