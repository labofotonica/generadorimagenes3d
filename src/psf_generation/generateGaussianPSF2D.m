% generateGaussianPSF2D
%
% Genera una PSF gaussiana 2D, devuelve intensidad y campo electrico.
% Recibe los parametros:
%   *w0 = cintura del haz.
%   *z = parametro z del haz.
%   *xp e yp = centros del haz.
%   *n = indice de refraccion del medio.
%   *lambda = longitud de onda del haz.
%   *resLateral = resolucion de la PSF.
%   *sz = tama�o de la PSF en pixeles.

function [PSF, E] = generateGaussianPSF2D(w0, z, xp, yp, n, lambda, resLateral, sz)
  z = z / n;
  zr = w0^2 * pi / lambda;
  w = w0 * sqrt(1+(z/zr)^2);
  k = 2*pi/lambda;
  unosobreR = z / (z^2+zr^2);
  
  [X, Y] = generateGrid([sz sz], [resLateral resLateral], true);
  
  R2 = (X-xp).^2 + (Y-yp).^2;
  E = w0/w * exp(-R2/w^2) .* exp(-1j*k*(z + R2*unosobreR/2));
  PSF = E .* conj(E);
end
