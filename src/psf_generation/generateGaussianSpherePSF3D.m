% generateGaussianSpherePSF3D
%
% Funcion que genera una esfera gaussiana 3D.
%
%   *sz: tama�o de la PSF 3D [z x y]
%   *sigma: vector con sigma en cada eje, puede pasarse como:
%       *[sx, sy, sz]
%       *[sxy, sz]
%       *[sxyz

function [salida, sigma] = generateGaussianSpherePSF3D(sz, sigma)
    
    %Asignacion de tama�os.
    sz_z = sz(1);
    sz_x = sz(2); 
    sz_y = sz(3);
    
    % Se distribuyen los valores de sigma de ser necesario.
    if(size(sigma,2) > 3)
        error('Sigma con mas de 3 componentes')
    elseif(size(sigma,2) == 2)
        sigma = [sigma(1) sigma(1) sigma(2)];
    elseif(size(sigma,2) == 1)
        sigma = [sigma(1) sigma(1) sigma(1)];
    end
    
    sigma_x = sigma(1);
    sigma_y = sigma(2);
    sigma_z = sigma(3);
   
    
    %Se crea el rango
    x = linspace(-sz_x / 2, sz_x/2, sz_x);
    y = linspace(-sz_y / 2, sz_y/2, sz_y);
    z = linspace(-sz_z / 2, sz_z/2, sz_z);
    
    [X,Z,Y] = meshgrid(x,z,y);
    
    % Se crea la PSF.
    psf = @(x,y,z,xo,yo,zo) exp(-0.5*((x-xo).^2./sigma_x.^2+(y-yo).^2./sigma_y.^2+(z-zo).^2./sigma_z.^2));
    
    salida = psf(X,Y,Z,0,0,0);
    
   
end