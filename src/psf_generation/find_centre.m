% find_centre
%
% Se le pasa un vector 3D con un campo electrico complejo y devuelve el
% indice del 'centro' que es calculado como el que minimo valor da la suma
% de sus valores absolutos.

function index = find_centre(X, dim)
    if(nargin < 2)
        dim = 1;
    end
    if dim > 3
        error('Dimension mayor a 3')
    end
    
    size_ = size(X);
    
    if dim ~= 1
        X = X(ceil(size_(1)/2), :, :);
    end
    if dim ~= 2
        X = X(:, ceil(size_(2)/2), :);
    end
    if dim ~= 3
        X = X(:, :, ceil(size_(3)/2));
    end
    
    X = squeeze(X);
    [~, index] = max(X);
end