% generatePSF3DWithMicroscPSF
%
% Genera una PSF 3D utilizando la funcion MicroscPSF. Recibe los
% parametros:
%   *params: estructura con los mismos parametros que pide MicroscPSF (ver
%   documentacion en la funcion). 
%   La unica diferencia es que en este caso el parametro pZ debe ser un
%   vector con los distintos pZ que se esperan si se desea hacer una PSF
%   por cada plano.
%   *search_factor: factor de busqueda utilizado para encontrar el centro
%   (recomendado usar 5). Se generan search_factor * sizeAxial planos para
%   poder encontrar el centro libremente.

function [E, PSF] = generatePSF3DWithMicroscPSF(params, search_factor)

    if(nargin < 2)
       search_factor = 5; 
    end
 
    if(size(params.pZ, 2) > 1)
        % params.resAxial = params.resAxial * search_factor; ver esto
        % para este caso
        pZ_vector = params.pZ;
        E = zeros(length(pZ_vector), params.size(1), params.size(2), params.size(3));
        sz = params.size;
        params.size = [sz(1), sz(2), sz(3) * search_factor];
        for i = 1 : length(pZ_vector)
            params.pZ = pZ_vector(i);
            E_parcial = MicroscPSF(params);
            index = find_centre(E_parcial);
            index1 = index + 1 - i;
            index2 = index - i + sz(3);
            inicio = min(index1, index2);
            fin = max(index1, index2); 

            E_parcial = E_parcial(:, :, inicio : fin);
            E(i, :, :, :) = E_parcial;
        end
        E = permute(E, [1, 4, 2, 3]);
        
    else
        sz = params.size;
        params.size = [sz(1), sz(2), sz(3) * search_factor];
        params.resAxial = params.resAxial * search_factor;
        E_parcial = MicroscPSF(params);
        centre = find_centre(E_parcial, 3);
        inicio = centre - floor(sz(3) / 2);
        fin = centre + ceil(sz(3) / 2) - 1;
        E_parcial = E_parcial(:, :, inicio : fin);
        E = permute(E_parcial, [3, 1, 2]);
    end
    
    
    PSF = abs(E).^2;
end