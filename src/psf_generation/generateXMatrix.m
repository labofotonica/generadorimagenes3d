% generateXMatrix
%
% Genera la matriz de propagacion ABCD para el sistema en el eje x. Devuelve tanto la
% matriz como tambien los parametros a b c y d por separado. Recibe los
% parametros:
%   *parte = 1 o 2. Define que parte del sistema refleja la matriz.
%   *params = struct con los parametros:
%       *f1 = distancia focal de la primera lente.
%       *f2 = distancia focal de la segunda lente.
%       *pZ = posicion de la particula en la muestra.
%       *n1 = indice de refraccion del medio.
%       *n2 = indice de refraccion de la lente inclinada f0.
%       *f0 = distancia focal de la lente inclinada.
%       *theta = angulo de inclinacion de la lente inclinada.
%       *camera_delta = distancia de la camara al punto de enfoque del
%       sistema sin la lente inclinada (solo f1 y f2).
%       *nm = indice de refraccion de la muestra.
%       *M = magnificacion del microscopio.

function [matrix, ax, bx, cx, dx] = generateXMatrix(parte, params)
  nof0 = 0;
  if ~isfield(params, 'f1')
    error('Falta f1');
  end
  if ~isfield(params, 'f2')
    error('Falta f2');
  end
  if ~isfield(params, 'pZ')
    params.pZ = 0;
  end
  if ~isfield(params, 'n1')
    params.n1 = 1;
  end
  if ~isfield(params, 'n2')
    params.n2 = 1;
  end
  if ~isfield(params, 'f0')
    nof0 = 1;
  end
  if ~isfield(params, 'theta')
     params.theta = 0;
  end
  if ~isfield(params, 'camera_delta')
    params.camera_delta = 0;
  end
  if ~isfield(params, 'nm')
    params.nm = 1.33;
  end
  if ~isfield(params, 'M')
    params.M = 60;
  end
  
  matrix_x = [1, 0; 0, 1];
  if(parte == 1)
    matrix_x = [1, params.f2; -1/params.f2, 0];
    if nof0 == 0
        matrix_x = matrix_x * [1, 0; -1/params.f0*(1+params.theta^2*(1+params.n1/2/params.n2)), 1];
    end
    matrix_x = matrix_x * [0, params.f1; -1/params.f1, 0];
    matrix_x = matrix_x * [-params.M, 0; 0, -1/params.M];
    matrix_x = matrix_x * [1, params.pZ/params.nm; 0, 1];
  end
  
  if(parte == 2)
    matrix_x = [1, params.camera_delta + params.f2; 0, 1];
  end;
  
  ax = matrix_x(1, 1);
  bx = matrix_x(1, 2);
  cx = matrix_x(2, 1);
  dx = matrix_x(2, 2);
  matrix = matrix_x;
end
