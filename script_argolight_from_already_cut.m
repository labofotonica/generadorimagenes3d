% script_argolight_from_already_cut.m

% Este script permite levantar muestras recortadas de argolight, agregarles la PSF
% y preparar la estructura de datos para correr en SUPPOSe.

clear all, close all;
clc;
addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot

% Archivo de entrada
input_folder = 'T500';

folder = 'Output/convolution';
output_filename = 'argolite';
distance = 1500;

%Se genera la PSF
inputPSF_folder = 'Output/PSF';
inputPSF_folder = fullfile(inputPSF_folder, '2021-11-25_10.33.48_PSF_MicroscPSF_3D_oversampled_2');
[params, PSF_work, sigma_work] = load_psf_file(inputPSF_folder);

PSF_work = PSF_work/max(PSF_work(:));

input_argolight = 'T500/muestras/horizontal_1500.mat';
load(input_argolight);

%%
sliceViewer(permute(salida, [2 3 1]), 'ColorMap', parula(256));

%% Guardado (solo guarda datos del plano observable).
check_folder(folder);

ext = '.mat';

PSF_filename = 'psf';
roi_filename = 'roi';

output_filename = [output_filename '_distance' num2str(distance) 'nm'];

[folder_name, timestamp] = generate_name_with_time(output_filename, '3D_vp');
folder_name = fullfile(folder, folder_name);
mkdir(folder_name)

roi_file = generate_name_with_time(roi_filename, output_filename, timestamp);
roi_file = fullfile(folder_name, [roi_file ext]);

PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
PSF_file = fullfile(folder_name, [PSF_file ext]);


% Se selecciona como roi solo el plano observable.
roi = salida(1:120, :, :);

% Se guardan solo los datos de sigma y de la PSF en el plano observable.
PSF = PSF_work;

pxSize = params.resLateral * 1000; %se pasa a nm para SUPPOSe
pxSizeAxial = params.resAxial * 1000;

sigma = sigma_work;
save(roi_file, 'roi', 'sigma', 'pxSize', 'pxSizeAxial');
save(PSF_file, 'PSF', 'sigma');