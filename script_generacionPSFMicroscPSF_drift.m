% script_generacion_PSFMicroscPSF_drift
%
% Este script de ejemplo genera una PSF a traves de la funcion MicroscPSF y
% le agrega drift.

clear, close all;
clc
addpath src/psf_generation
addpath src/aux_functions
addpath src/plot

%Archivo de salida
save_data = true;
folder = 'Output/PSF/';
output_filename = 'MicroscPSF_3D_beads';

NA = 1.45;
ti0 = 120; %micrones
ns = 1;
lambda = 0.525; %micrones
pxSizeLateral = 0.0542; %um
pxSizeAxial = 0.0347; %um

% Parametros de calculo
sizeLateral = 100;
sizeAxial = 300; 

extraSizeDrift = 0;

%% Generacion PSF.
sample_depth = sizeAxial * pxSizeAxial;

pZ = 0;

%Parametros para generacion de la PSF (MicroscPSF)
params.size = [sizeLateral + extraSizeDrift sizeLateral + extraSizeDrift sizeAxial];
params.NA = NA;
params.resLateral = pxSizeLateral * 1e-6;
params.resAxial = pxSizeAxial * 1e-6;
params.sizeLateral = sizeLateral + extraSizeDrift;
params.sizeAxial = sizeAxial;
params.pZ = pZ * 1e-6;
params.lambda = lambda * 1e-6;
params.ns = ns;
params.ti0 = 110 * 1e-6;
params.tg = 150e-6;
params.tg0 = 150e-6;
params.M = 60;

PSF = MicroscPSF(params);
PSF = permute(PSF, [3 1 2]);
PSF = PSF / max(PSF(:));

sigma = [sqrt(2) * lambda / 4 / NA / pxSizeLateral sqrt(2) * lambda / 4 / NA / pxSizeLateral sqrt(2) * lambda / 4 / NA / pxSizeAxial];

%% Agrego drift
drift = [-0.575 0.061]; %px/imagen
PSF_drift = PSF;
for i = 1 : sizeAxial
    d = i - ceil(sizeAxial/2);
    PSF_drift(i, :, :) = imtranslate(squeeze(PSF_drift(i, :, :)), drift * d);
end

% recorto
PSF = PSF_drift(:, ceil(size(PSF, 2) / 2) - floor(sizeLateral/2) : ceil(size(PSF, 2) / 2) + floor(sizeLateral/2), ceil(size(PSF, 2) / 2) - floor(sizeLateral/2) : ceil(size(PSF, 2) / 2) + floor(sizeLateral/2));
%%
figure()
plotLateral(squeeze(PSF(50, :, :)))
colorbar

figure()
imagesc(squeeze(PSF(:, :, ceil(size(PSF, 3) / 2))))
%% Guardado de parametros para convolucion y de PSF.
if save_data
    check_folder(folder)
    ext = '.mat';
    add = 'PSF';

    params_conv_filename = 'params_conv';
    PSF_filename = 'PSF';

    [folder_name, timestamp] = generate_name_with_time(output_filename, add);
    folder_name = fullfile(folder, folder_name);
    mkdir(folder_name);

    params_conv_file = generate_name_with_time(params_conv_filename, output_filename, timestamp);
    params_conv_file = fullfile(folder_name, [params_conv_file ext]);

    PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
    PSF_file = fullfile(folder_name, [PSF_file ext]);

    params_conv.sizeLateral = sizeLateral;
    params_conv.sizeAxial = sizeAxial;
    params_conv.resLateral = pxSizeLateral;
    params_conv.resAxial = pxSizeAxial;

    save(params_conv_file, 'params_conv')
    save(PSF_file, 'PSF', 'sigma', 'pxSizeAxial', 'pxSizeLateral')
end