% script_procesar_medicion.m

% Este script permite procesar mediciones del microscopio y prepararlas
% para correr en SUPPOSe.

clear all, close all;
clc;
addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot

% Archivo de entrada
general_input_folder = 'C:\Users\matia\Desktop\Datos_beads\2023-01-18_Stacks_BlueLaser_TIRF_beads_500nm\';
input_folder = '2023-01-18_18.47.57_Stack_3D_60x2xNA1.45_BlueLaser_899.9911msRS';
%input_folder = '2022-03-09_16.29.52_Beads_500nm_60x1.6x2xNA1.45_LedUV_600.011msRS';
input_file = 'MMStack_Pos0.ome.tif';

% Psf entrada
%inputPSF_folder = 'Output/PSF';
%inputPSF_folder = fullfile(inputPSF_folder, '2022-03-11_Argolight-lineas-medidas\2022-03-11_14.06.53_PSF_MicroscPSF_3D_Argolight-lineas-medidas-os2_NA1.45_lambda0.46_nm1.55');
inputPSF_folder = 'C:\Users\matia\Desktop\Datos_beads\2023-01-18_Stacks_BlueLaser_TIRF_beads_100nm\2023-01-18_17.15.53_Stack_3D_60x2xNA1.45_BlueLaser_5000.007msRS\2023-01-18_19.17.13_AjustePSF\2023-01-18_19.17.13_PSF_beads_100nm_texp_5000_60x2_NA1.45_TIRF_gaussfi0.5';

% Archivo de salida
output_folder = 'Output/procesado/2023-01-18_Beads_500nm_Laser_TIRF';
output_filename = 'Beads_500nm_Laser_PSF_1bead_0.5gaussfil'; 

padding_z = true;

%% Generar stack
stack = load_stack_from_tiff(fullfile(general_input_folder, input_folder, input_file));
pxSizeLateral = 0.054;%um
pxSizeAxial = 0.0347;%um

%%
figure()
sliceViewer(permute(stack,[2 3 1]), 'ColorMap', parula(256));
colorbar
axis image

%% Descomentar para corregir drift
%[stack, drift] = drift_correction_max_method(stack, 10, 40, [0.7, -1.0333]);
% 
% sliceViewer(permute(stack2,[2 3 1]), 'ColorMap', parula(256))
% colorbar
% axis image


%% Carga de PSF
%Se genera la PSF
[params, PSF_work, sigma_work] = load_psf_file(inputPSF_folder);

PSF_work = PSF_work/max(PSF_work(:));

% PSF_work = padarray(PSF_work, [0, 64 - size(PSF_work, 2), 64 - size(PSF_work, 3)], 'replicate', 'post');
% 
% %%%% Padding en z
% if padding_z
% 	PSF_work = padarray(PSF_work, [64 - size(PSF_work, 1), 0, 0], 0, 'post');
% end
figure
sliceViewer(permute(PSF_work, [2 3 1]), 'ColorMap', parula(256));
colorbar
%%
% sizeAxial = params.sizeAxial;
sizeAxial = ceil(params.sizeAxial/2);
sizeLateral = ceil(params.sizeLateral/2);
resLateral = params.resLateral;
resAxial = params.resAxial;

observablePlanePSF = ceil(params.sizeAxial/2) + 1;

%%
sliceViewer(permute(stack, [2 3 1]), 'Colormap', parula(256));
h_rect = imrect();
pos_rect = h_rect.getPosition();
pos_rect = round(pos_rect);
%pos_rect(3) = 63;
%pos_rect(4) = 63;
% Agregar para que siempre ponga primero el minimo porque sino te pone la
% imagen al reves.
%salida = stack(:, pos_rect(2) + (0:pos_rect(4)), pos_rect(1) + (0:pos_rect(3)));
%% salida harcodeada
salida = stack(:, 775:826, 1705:1797); 

%%%% Padding en z
% if padding_z
% 	salida = padarray(salida, [64 - size(salida, 1), 0, 0], 0, 'post');
% end

%%
figure()
imagesc(squeeze(sum(salida, 2)).');
xlabel('z[px]')
ylabel('x[px]')
colorbar
title('ROI')

figure()
imagesc(squeeze(sum(salida, 3)).');
xlabel('z[px]')
ylabel('y[px]')
colorbar
title('ROI')

%%
figure()
imagesc(squeeze(sum(PSF_work, 2)).');
xlabel('z[px]')
ylabel('x[px]')
colorbar
title('PSF')

figure()
imagesc(squeeze(sum(PSF_work, 3)).');
xlabel('z[px]')
ylabel('y[px]')
colorbar
title('PSF')

%%
figure
sliceViewer(permute(salida, [2 3 1]), 'ColorMap', parula(256));

%% Guardado (solo guarda datos del plano observable).
check_folder(output_folder);

ext = '.mat';

PSF_filename = 'psf';
roi_filename = 'roi';

output_filename = [output_filename];

[folder_name, timestamp] = generate_name_with_time(output_filename, '3D_vp');
folder_name = fullfile(output_folder, folder_name);
mkdir(folder_name)

roi_file = generate_name_with_time(roi_filename, output_filename, timestamp);
roi_file = fullfile(folder_name, [roi_file ext]);

PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
PSF_file = fullfile(folder_name, [PSF_file ext]);

roi_from = [pos_rect(2), pos_rect(1)];
roi_size = [pos_rect(4), pos_rect(3)];

% Se selecciona como roi solo el plano observable.
roi = salida(:, :, :);

% flipeo
%roi = flip(roi, 1);

%%

% Se guardan solo los datos de sigma y de la PSF en el plano observable.
PSF = PSF_work;

pxSize = params.resLateral * 1000; %se pasa a nm para SUPPOSe
pxSizeAxial = params.resAxial * 1000;

sigma = sigma_work;
save(roi_file, 'roi', 'sigma', 'pxSize', 'pxSizeAxial', 'roi_from', 'roi_size', 'input_folder');
save(PSF_file, 'PSF', 'sigma');