% script_ejemplo_generacionPSFMicroscPSF
%
% Este script de ejemplo genera una PSF a traves de la funcion MicroscPSF y
% la propaga utilizando la integral de fresnel (con la funcion
% fresnelABCDIntegral).

clear, close all;
clc
addpath src/psf_generation
addpath src/aux_functions
addpath src/plot

%Archivo de salida
save_data = true;
folder = 'Output/PSF/crossed_lines_tesis';
output_filename = 'MicroscPSF_3D';

%Parametros del microscopio y muestra
NA = 1.45;
M = 60;
ti0 = 120; %micrones
nm = 1;
lambda = 0.5; %micrones
pxSizeLateral = 0.054;%um
pxSizeAxial = 0.035;%um

output_filename = [output_filename '_NA' num2str(NA) '_lambda', num2str(lambda) '_nm' num2str(nm)];

% Parametros de calculo
sizeLateral = 51;
sizeAxial = 101;

%% Configuracion para generacion de PSF y propagacion
sample_depth = sizeAxial * pxSizeAxial;

pZ = sample_depth / 2; % PSF en la mitad de l la muestra

%Parametros para generacion de la PSF (MicroscPSF)
params.size = [sizeLateral sizeLateral sizeAxial];
params.NA = NA;
params.M = M;
params.resLateral = pxSizeLateral * 1e-6;
params.resAxial = pxSizeAxial * 1e-6;
params.sizeLateral = sizeLateral;
params.sizeAxial = sizeAxial;
params.pZ = pZ * 1e-6;
params.lambda = lambda * 1e-6;


%% Generacion de la PSF
[~, PSF] = generatePSF3DWithMicroscPSF(params);
PSF = PSF / max(PSF(:));
figure()
plotAxial(permute(PSF, [2, 3, 1]), [], 'x')
axis off
colorbar off
sigma = sigma_estimation(PSF, 'fit');
set(gca,'YDir','reverse')

%%
figure()
plotLateral(squeeze(PSF(40, :, :)))
colorbar
%% Guardado de parametros para convolucion y de PSF.
if save_data
    check_folder(folder)
    ext = '.mat';
    add = 'PSF';

    params_conv_filename = 'params_conv';
    PSF_filename = 'PSF';

    [folder_name, timestamp] = generate_name_with_time(output_filename, add);
    folder_name = fullfile(folder, folder_name);
    mkdir(folder_name);

    params_conv_file = generate_name_with_time(params_conv_filename, output_filename, timestamp);
    params_conv_file = fullfile(folder_name, [params_conv_file ext]);

    PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
    PSF_file = fullfile(folder_name, [PSF_file ext]);

    params_conv.sizeLateral = sizeLateral;
    params_conv.sizeAxial = sizeAxial;
    params_conv.resLateral = pxSizeLateral;
    params_conv.resAxial = pxSizeAxial;

    save(params_conv_file, 'params_conv')
    save(PSF_file, 'PSF', 'sigma', 'pxSizeAxial', 'pxSizeLateral')
end