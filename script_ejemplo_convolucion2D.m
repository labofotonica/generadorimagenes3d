% script_ejemplo_convolucion2D
%
% Este script de ejemplo genera distintas formas para luego convucionarlas
% con una PSF 2D de entrada que se genera en el mismo script.

clear, close all;

addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot
addpath src/psf_generation

%Archivo de salida
folder = 'Output/convolution';
output_name = 'convolucion2D';

%Parámetros del microscopio y de resolución.
NA = 1.4;
M = 60;
lambda = 0.5; %micrones
w0 = 10.44; %micrones

sample_width = 0.625; %micrones
sizeLateral = 25;


resLateralMuestra = sample_width / sizeLateral;
resLateral = resLateralMuestra * M;
sigma = w0 / 2 / resLateral;
alpha = 100;

%Ruido y fondo
background_value = 0;
background_mode = 'constant';

snr_esperada = inf;
noise_distribution = 'gaussian';

%% Generacion de grid y PSF gaussiana
[~, ~, xPixel, yPixel] = generateGrid([sizeLateral sizeLateral], [1 1], false);

% Se genera la PSF gaussiana.
PSF = generateGaussianPSF2D(w0, 0, 0, 0, 1.33, lambda, resLateral, sizeLateral);
PSF = PSF / max(max(PSF));

%Plot de la PSF
%Plot de PSF.
figure()
imagesc(xPixel, yPixel, PSF)
title('PSF | sigma = ' + string(sigma) + ' px | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'px | tamPixel \approx ' + string(resLateral) + ' um');
colorbar
axis square

%% Generacion de la entrada
% Las funciones generate suman, es decir que es acumulativo el agregado de
% formas. 
% Devuelven las posiciones en el rango [1, sz(entrada)].

% Definicion de vectores.
entrada = zeros(sizeLateral, sizeLateral);
posiciones = [];

%Ejemplos
%[entrada, posiciones] = generateDot2D(entrada, posiciones, 100, [13 13], 1, 100, 'gaussian');
[entrada, posiciones] = generateLine2D(entrada, posiciones, 100, [10 10], [16 10], 1000); 
%[entrada, posiciones] = generatePimpedTwoLines2D(entrada, posiciones, 2, 100, 100);


% Plot de entrada
figure()
plotImageAndParticles(entrada, posiciones)
title('Particulas y pixeles pintados | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'px | tamPixel \approx ' + string(resLateral) + ' um')
ylabel('y [pixeles]')
xlabel('x [pixeles]')


%% Convolucion y obtencion de salida con ruido y fondo
salida = convn(PSF, entrada, 'same');

[salida, background] = addBackground(salida, background_value, background_mode);
[salida, noise] = addNoise(salida, snr_esperada, noise_distribution);
snr_salida = snr(salida, noise);
background_salida = background(1);


figure()
plotImageAndParticles(salida, posiciones)
title('Convolucion | sigma = ' + string(sigma) + ' px | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'px | tamPixel \approx ' + string(resLateral) + [' um' newline 'background = '] + string(background_salida) + ' | SNR = ' + string(snr_salida))
ylabel('y')
xlabel('x')

%% Guardado
check_folder(folder)
ext = '.mat';
output_filename = generate_name_with_time(output_name, '2D');
file = fullfile(folder, output_filename);

xExacto = squeeze(posiciones(:, 1)).';
yExacto = squeeze(posiciones(:, 2)).';
positions = posiciones;

pxSize = resLateral * 1000; %Se pasa a nm para SUPPOSe

roi = salida;

mkdir(file)

file = fullfile(file, output_filename);

save([file '_roi' ext], 'roi', 'sigma', 'xExacto', ...
'yExacto', 'alpha', 'pxSize', 'background', 'noise', ...
'background_salida', 'snr_salida', 'positions');

save([file '_psf' ext], 'PSF', 'sigma');
