% script_argolight

% Este script permite recortar las muestras de argolight, agregarles la PSF
% y preparar la estructura de datos para correr en SUPPOSe.

clear, close all;
addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot

% Archivo de entrada
input_folder = 'T500';


% Archivo de salida
folder = 'Output/convolution';
output_filename = 'argolite'; 

%% Preparo el stack
listing = dir(fullfile(input_folder, '/*Cruces3D*'));
listing = listing(3:end);

totalstacksize = 0;
totallateralsize = [0 0];

file = dir(fullfile(input_folder, listing(1).name, 'image_no_drift.tif'));
info = imfinfo(fullfile(input_folder, listing(1).name, file(1).name));
stacksize = size(info, 1);

width = info.Width;
height = info.Height;
width = width(1);
height = height(1);
totallateralsize = [width, height];

stack = zeros( stacksize, height, width);

for j = 1 : stacksize
   stack(j, :, :) = imread(fullfile(input_folder, listing(1).name, file(1).name), j);
end

totalstacksize = totalstacksize + stacksize;

for i = 2 : size(listing, 1)
    file = dir(fullfile(input_folder, listing(i).name, '*.tif'));
    info = imfinfo(fullfile(input_folder, listing(i).name, file(1).name));
    stacksize = size(info, 1);
    stack = [stack; zeros(stacksize, height, width)];
    for j = 1 : stacksize
        stack(j + totalstacksize, :, :) = uint16(imread(fullfile(input_folder, listing(i).name, file(1).name), j));
    end
    totalstacksize = totalstacksize + stacksize;
end
clear file

%%
sliceViewer(permute(stack, [2 3 1]));


%% Selecciono parte de la matriz
xInicio = 369;
xFin = 1603;
yInicio = 562;
yFin = 1631;

stack2 = stack(:, yInicio:yFin, xInicio:xFin);

save(fullfile(input_folder, 'image.mat'), 'stack2');

file = fullfile(input_folder, 'image.tiff');
%%
save_stack_as_tiff(stack2, file);
