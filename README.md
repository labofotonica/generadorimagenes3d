# GeneradorImagenes3D


Este programa genera muestras sintéticas 2D y 3D para poder trabajar luego con SUPPOSe, asi como tambien permite generar distintas PSFs para la obtención de estas muestras.

## Generacion de PSFs

El programa cuenta con distintas funciones utilizadas para generar distintos tipos de PSF. Y tambien para propagarlas a través de un sistema de matrices ABCD. Estas funciones se encuentran en la carpeta ```src/psf_generation ```.

* **generateGaussianPSF2D**: Permite generar una PSF gaussiana en 2D.
* **generateGaussianPSF3D**: Permite generar una PSF gaussiana en 3D.
* **generateAndPropagateGaussianPSF3D**: Permite generar un stack de PSF gaussianas 3D. Se genera una PSF distinta para cada plano de la muestra. Es decir, si se pide una PSF de depth x width x width, se va a obtener una PSF '4D' de tamaño depth x depth x width x width. Esto es útil para realizar la propagación, donde la PSF va a variar mucho a lo largo de la profundidad de la muestra. Esta función además, como indica el nombre, se encarga de realizar la propagación, por lo que se le deben pasar las funciones correspondientes a las matrices ABCD en X y en Y.
* **MicroscPSF**: Función externa que permite generar una PSF téorica a partir del modelo de Gibson-Lanni (modificada para que devuelva campo eléctrico).
* **FresnelABCDIntegral**: Permite propagar una PSF de la que se tengan valores numéricos del campo eléctrico (usada en combinación con MicroscPSF) a traves de la integral de Fresnel para sistemas ABCD.



Ejemplos donde se muestra la utilización de estas funciones mencionadas son:
```
script_ejemplo_generacionPSFGaussiana3D_constante.m
script_ejemplo_generacionPSFGaussiana3D_variable.m
script_ejemplo_generacionPSFMicroscPSF_varialbe.m
script_ejemplo_generacionPSFMicroscPSF_sinprop_constante.m
```

## Generador de formas y convolución

El programa contiene funciones para la generacion de formas y para la convolucion de las muestras generadas con las PSFs generadas (o externas). 

Estas funciones se encuentran en la carpeta ```src/convolution/shape_generators```. Las formas son generadas a traves de particulas, cuyas posiciones también son devueltas. Es posible elegir la cantidad de particulas y el valor alfa de ellas.

* **generateDot**: Permite generar un punto en la posición indicada. Se puede generar un punto consistente de una sola particula o una distribución gaussiana o uniforme de ellas.
* **generateLine**: Permite generar una linea entre dos puntos dados.
* **generatePimpedTwoLines**: Permite generar dos lineas inclinadas 45° y separadas una distancia dada. Estas lineas solamente pueden estar contenidas en un plano en el eje z.
* **generateMicrotubules3D**: Permite generar microtubulos sintéticos (utiliza el paquete de Python ```pysuppose_tools```).

Estas funciones también tienen versiones 2D que dependen de las enumeradas anteriormente:
* **generateDot2D**
* **generateLine2D**
* **generatePimpedTwoLines2D**
* **generateMicrotubules2D** (utiliza el paquete de Python ```pysuppose_tools```)

El resto de funciones relacionadas con la convolucion se encuentran en ```src/convolution```.

* **convolvePSF3D**:
* **addBackground**: Permite agregar fondo a las imagenes ya convolucionadas.
* **addNoise**: Permite agregar ruido a las imagenes ya convolucionadas.
* **generateInitialPopulation**: Permite generar una poblacion inicial para SUPPOSe a partir de las posiciones de las particulas del ground truth.

Ejemplos donde se muestra la utilización de estas funciones mencionadas son:
```
script_ejemplo_convolucion2D.m
script_ejemplo_convolucion3D_solo_un_plano_PSF_variable.m
script_ejemplo_convolucion3D_varios_planos_PSF_constante.m
script_ejemplo_convolucion3D_varios_planos_PSF_variable.m
```


## Gráficos

Se tienen además funciones para graficar los resultados obtenidos. Estas se encuentran en ```src/plot```. Estas son:

* **plotParticles**: Permite graficar particulas dadas sus posiciones 2D.
* **plotImageAndParticles**: Permite graficar una imagen 2D y sobre esta las particulas que la generaron.
* **plotAxial**: Permite graficar la intensidad de una imagen a lo largo del eje axial.
* **plotParticlesAxial**: Permite graficar particulas dadas sus posiciones sobre el eje axial.
* **plotImageAndParticles**: Permite graficar una imagen sobre el eje axial y encima de ella las particulas que la generaron.
* **plot4d**: Permite graficar en "4 dimensiones", utilizando color para identificar en profundidad (eje z) y transparencia para mostrar intensidad.

## Funciones Auxiliares

Se tienen algunas funciones auxiliares para distintos usos. Referidas principalmente al manejo de archivos. Además, se tiene:

* **GenerateGrid**: Permite generar dos vectores de coordenadas y devuelve además dos vectores con estas coordenadas hechas una grilla.

## Dependencias

Para utilizar las funciones de generación de microtubulos, se debe tener instalado el paquete de Python  ```pysuppose_tools```, que a su vez requiere los paquetes ```catmu``` y ```pysuppose```.

## Recomendaciones

Es recomendable que tanto las PSFs como las muestras tengan tamaños impares, para una correcta convolución y visualización de gráficos, pudiendose luego quitar el sobrante de la imagen de ser necesario.
