
% script_ejemplo_convolucion3D
%
% Este script permite generar las rectas cruzadas con su PSF corrida para
% probar el comportamiento de los beads que no funcionan.

clear, close all;
clc
addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot
addpath src/psf_generation

%Archivo de salida
folder = 'Output/convolution/2023-01-23_Analisis-corrimiento-PSF-z';
folder = 'Output/convolution/2023-01-24_Analisis-tridimensionalidad-PSF';
output_filename = 'bead_30'; 

%Se genera la PSF
%inputs_folder = 'Output/PSF';
%input_folder = '2022-03-08_Argolight-lineas/2022-03-08_12.01.07_PSF_MicroscPSF_3D_Argolight-lineas_os1_NA1.45_lambda0.46_nm1.55';
%input_folder = '2022-06-09_14.47.57_PSF_gaussian_3D_constante_astig';
%[params, PSF_work, sigma_work] = load_psf_file(fullfile(inputs_folder, input_folder));

extra_generation_size = 50; % Extra que se genera de la PSF que se va a usar.
corrimientos_a_generar = [0];%, 5, 10, 15, 20];

params.sizeLateral = 51;
params.sizeAxial = 51;
params.resLateral = 0.0542; %um
params.resAxial = 0.035; %um

sizeLateral = params.sizeLateral;
sizeAxial = params.sizeAxial;
resLateral = params.resLateral;
resAxial = params.resAxial;

params_psf.sample_depth = (params.sizeAxial + extra_generation_size) * params.resAxial;
params_psf.lambda = 0.5;
params_psf.w0 = 0.2;
params_psf.sample_width = params.sizeLateral * params.resLateral;
params_psf.nm = 1;
params_psf.z = params_psf.sample_depth / 2;




[PSF_total, sigma_work] = generateGaussianPSF3D([params.sizeAxial + extra_generation_size params.sizeLateral params.sizeLateral], params_psf);

PSF_work = PSF_total(1 + floor(extra_generation_size/2) : sizeAxial + floor(extra_generation_size/2), :, :);

% Particulas
alpha = 100;

%Ruido y fondo
background_value = 0;
background_mode = 'constant';

snr_esperada = Inf;
noise_distribution = 'gaussian';

%% Generacion de coordenadas para graficos
[~, ~, xPixel, yPixel] = generateGrid([sizeLateral sizeLateral], [1 1], false);
zPixel = linspace(1, sizeAxial, sizeAxial);

%% Plot de la PSF axial.

figure()
plotAxial(PSF_work, [], 'x')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

figure()
plotAxial(PSF_work, [], 'y')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');


%% Generacion de la entrada.
% Las funciones generate suman, es decir que es acumulativo el agregado de
% formas. 
% Devuelven las posiciones en el rango [1, sz(entrada)].
entrada = zeros(sizeAxial, sizeLateral, sizeLateral);
positions = [];
multiplicacion = 2;
center = ceil(sizeAxial/2);
primero = center - floor(sigma_work(3)*multiplicacion/2);
segundo = center + ceil(sigma_work(3)*multiplicacion/2);
% 
in = [ceil(33/2) ceil(33/2) 0];
out = [ceil(sizeLateral/2) ceil(sizeLateral/2) 0];

%[entrada, positions] = generateDot(entrada, positions, 2000, [26, 26, 26], 16, 100, 'uniform');
[entrada, positions] = generateEllipsoid(entrada, positions, 1000, [26, 26, 26], [16, 16, 16], 100);
%[entrada, positions] = generateLine(entrada, positions, 100, [1, 1, 1], [sizeLateral sizeLateral sizeAxial], alpha);
%[entrada, positions] = generateLine(entrada, positions, 100, [5, 5, primero] - in + out,[29, 29, primero]- in + out, alpha); %3 sigma
%[entrada, positions] = generateLine(entrada, positions, 100, [29, 5, segundo]- in + out,[5, 29, segundo]- in + out, alpha); %3 sigma

%% Plot ejes laterales con entrada y particulas
figure()
plotImageAndParticles(squeeze(sum(entrada(:, :, :),1)), positions(:, 1:2));
title(['Entrada en plano observable | ' ('size = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'px | sizePixelLat \approx ' + string(resLateral) + ' um')]);

%%
figure()
x = positions(:, 1);
y = positions(:, 2);
z = positions(:, 3);
scatter(x, y, [], z, 'filled')
xlim([1 sizeLateral])
ylim([1 sizeLateral])
caxis([1 sizeAxial])
colorbar
axis square
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);


%%
figure()
plot3d(positions, [1 sizeLateral 1 sizeLateral 1 sizeAxial], 'b.')
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

%% Plot ejes axiales con entrada y particulas.
figure()
plotImageAndParticlesAxial(entrada, [], 'x', positions, 'sum')
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

figure()
plotImageAndParticlesAxial(entrada, [], 'y', positions, 'sum')
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

%% Convolucion y agregado de ruido y fondo.
salida = convn(entrada, PSF_work, "same");
[salida, background] = addBackground(salida, background_value, background_mode);
[salida, noise] = addNoise(salida, snr_esperada, noise_distribution);
%noise = zeros(size(salida));
snr_salida = snr(salida, noise);
background_salida = background(1);

%%
figure
obs = ceil(sizeAxial/2);
imagesc(squeeze(salida(obs, :, :)))
axis square
colorbar

% %% Plot en el plano observable.
% figure()
% plotImageAndParticles(squeeze(salida(observablePlane, :, :)), positions(:, 1:2))
% tit = {['Salida en plano observable | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
% title(tit);

%% Plot 4d convolucion
figure()
params_4d.xlabel = 'x [px]';
params_4d.ylabel = 'y [px]';
params_4d.zlabel = 'z [px]';
params_4d.backgroundalpha = 1;
params_4d.FontSize = 12;
plot4d(salida, params_4d)
    title(['Convolucion | sigma = ' num2str(sigma_work) ' px'])

clear params_4d

%% Plot lateral con salida y particulas.
figure()
plotImageAndParticlesAxial(salida, [], 'x', positions, 'sum')
tit = {['Salida convolucionada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

figure()
plotImageAndParticlesAxial(salida, [], 'y', positions, 'sum')
tit = {['Salida convolucionada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);
%% Generacion de poblacion inicial
initial_population = generateInitialPopulation(positions, 100, min(sigma_work(:)) * 3);

%% Guardado (solo guarda datos del plano observable).
check_folder(folder);

ext = '.mat';

PSF_filename = 'psf';
roi_filename = 'roi';
roiPlane_filename = 'roiPlane';
initial_population_filename = 'initial_population';

for i = 1 : length(corrimientos_a_generar)
    final_output_filename = [output_filename num2str(corrimientos_a_generar(i)) '_snr' num2str(snr_salida, 1) '_back' num2str(background_salida, 1)];

    [folder_name, timestamp] = generate_name_with_time(final_output_filename, '3D_vp');
    folder_name = fullfile(folder, folder_name);
    mkdir(folder_name)

    roi_file = generate_name_with_time(roi_filename, final_output_filename, timestamp);
    roi_file = fullfile(folder_name, [roi_file ext]);

    PSF_file = generate_name_with_time(PSF_filename, final_output_filename, timestamp);
    PSF_file = fullfile(folder_name, [PSF_file ext]);



    initial_population_file = generate_name_with_time(initial_population_filename, final_output_filename, timestamp);
    initial_population_file = fullfile(folder_name, [initial_population_file ext]);

    % Se selecciona como roi solo el plano observable.
    roi = salida;

    zExacto = squeeze(positions(:, 3)).';
    xExacto = squeeze(positions(:, 1)).';
    yExacto = squeeze(positions(:, 2)).';

    % PSF corrida
    PSF = PSF_total(1 + corrimientos_a_generar(i) + floor(extra_generation_size/2) : sizeAxial + corrimientos_a_generar(i) + floor(extra_generation_size/2), :, :);
    
    % Se guardan solo los datos de sigma y de la PSF en el plano observable.
    pxSize = params.resLateral * 1000; %se pasa a nm para SUPPOSe
    pxSizeAxial = params.resAxial * 1000;
    sigma = sigma_work;

    save(roi_file, 'roi', 'positions', 'sigma', 'zExacto', 'xExacto', 'yExacto', 'alpha', 'pxSize', 'pxSizeAxial', 'snr_salida', 'background_salida');
    save(PSF_file, 'PSF', 'sigma');
    save(initial_population_file, 'initial_population');
end

