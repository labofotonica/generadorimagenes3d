% script_ejemplo_generacionPSFGaussiana3D
%
% Este script de ejemplo genera una PSF gaussiana 3D generada y propagada
% por la funcion generateAndPropagateGaussian3DPSF.

clear, close all;
addpath src/psf_generation
addpath src/aux_functions
addpath src/plot

%Archivo de salida
folder = 'Output/PSF';
output_filename = 'gaussian_3D_constante_astig';

sizeLateral = 65; %impar para no generar problemas de visualizacion.
sizeAxial = 65; %impar para no generar problemas de visualizacion.

%Parametros del sistema
f1 = 0.1; %metros
f2 = 0.2; %metros
f0 = 0.4; %metros
n1 = 1; %indice de refracción del medio
n2 = 1.5; %indice de refracción de la lente f0.
theta = 0.23267; 
nm = 1.33;
M = 60;
m = 2;


sample_depth = 1; % micrones
sample_width = 300; %micrones (para propagar)
%sample_width = 1; %micrones (sin propagacion)
lambda = 0.5; % micrones
w0 = 5.21983261434838 * 2 / M; %micrones
pZ = sample_depth/2;



%Parametros de propagacion
%camera_delta = -0.107218; %metros
camera_delta = -f2^2 * (2*f1^2*n2*nm + f0 * M^2 * (2*n2*sample_depth*1e-6/2 + n1 * (sample_depth*1e-6/2))) / (2 * f0 * f1^2 * n2 * nm);
observable_plane = ceil(sizeAxial/2); %Para camara en el medio, hacer bien el calculo automático con delta camara.


%%
params_matrix.f1 = f1 * 1e6;
params_matrix.f2 = f2 * 1e6;
params_matrix.f0 = f0 * 1e6;
params_matrix.n1 = n1;
params_matrix.n2 = n2;
params_matrix.nm = nm;
params_matrix.M = M;
params_matrix.theta = theta;
params_matrix.camera_delta = camera_delta * 1e6;
params_matrix.m = m;


params_gaussian.sample_depth = sample_depth;
params_gaussian.lambda = lambda;
params_gaussian.w0 = w0; %micrones
params_gaussian.sample_width = sample_width;
params_gaussian.nm = nm;
params_gaussian.pZ = pZ;


%% Se llama a la funcion de generacion
[PSF, sigma] = generateAndPropagateGaussianPSF3D2([sizeAxial, sizeLateral, sizeLateral], params_gaussian, params_matrix, @generateXMatrix, @generateYMatrix);
%[PSF, sigma] = generateAndPropagateGaussianPSF3D([sizeAxial, sizeLateral, sizeLateral], params_gaussian);


%% Ploteo en eje axial.
figure()
plotAxial(PSF, [1 sizeAxial 1 sizeLateral], 'x', 'sum')

figure()
plotAxial(PSF, [1 sizeAxial 1 sizeLateral], 'y', 'sum')

%% Ploteo en el eje lateral.
for i = 20 : 40
    figure()
    imagesc(squeeze(PSF(i, :, :)))
    axis square
    colorbar
end



%% Guardado de parametros para convolucion y de PSF.
check_folder(folder)
ext = '.mat';
add = 'PSF';

params_conv_filename = 'params_conv';
PSF_filename = 'PSF';
PSFcut_filename = 'PSFcut';

[folder_name, timestamp] = generate_name_with_time(output_filename, add);
folder_name = fullfile(folder, folder_name);
mkdir(folder_name);

params_conv_file = generate_name_with_time(params_conv_filename, output_filename, timestamp);
params_conv_file = fullfile(folder_name, [params_conv_file ext]);

PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
PSF_file = fullfile(folder_name, [PSF_file ext]);

params_conv.sizeLateral = sizeLateral;
params_conv.sizeAxial = sizeAxial;
params_conv.resLateral = sample_width / sizeLateral;
params_conv.resAxial = sample_depth / sizeAxial * (m * M)^2;
params_conv.pZ_vector = linspace(0, sample_depth, sizeAxial);
params_conv.observable_plane = observable_plane;

save(params_conv_file, 'params_conv')
save(PSF_file, 'PSF', 'sigma')