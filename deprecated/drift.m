clear all, close all;
clc;
addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot

% Archivo de entrada
input_folder = 'C:/Users/Matias/Desktop/2022-02-22-Argolight_Lineas_3D/';
folder_filename = '2022-02-22_17.24.33_Argoligth-Lineas-3D_60x1.6x2xNA1.45_LedUV_2000.0028msRS';
input_file = 'MMStack_Pos0.ome.tif';

tiff_to_mat(fullfile(input_folder, folder_filename, 'MMStack_Pos0.ome.tif'), fullfile(input_folder, folder_filename, 'MMStack_Pos0.mat'));
load(fullfile(input_folder, folder_filename, 'MMStack_Pos0.ome.mat'));
%%
sliceViewer(permute(stack2, [2 3 1]), 'Colormap', parula(256));
h_rect = imrect();
pos_rect = h_rect.getPosition();
pos_rect = round(pos_rect);
salida = stack2(:, pos_rect(2) + (0:pos_rect(4)), pos_rect(1) + (0:pos_rect(3)));


%%
a = [];
stack3 = salida;
for i = 1 : size(stack3,1)
    im = squeeze(stack3(i, :, :));
    [C,I] = max(im(:));
    [a0, a1] = ind2sub(size(im),I);
    a = [a; [a0, a1]];
    stack3(i, a0, a1) = C * 100;
end

figure
sliceViewer(permute(stack3,[2 3 1]), 'ColorMap', parula(256))
colorbar

%%
figure
plot(a(1:53, 1), a(1:53, 2), 'r.')
%%
drift_ = mean(a(1:52, :) - a(2:53, :));
drift_ = [drift_(2) drift_(1)];
stack4 = zeros(size(stack2));
for i = 1 : size(stack3, 1)
    stack4(i, :, :) = imtranslate(squeeze(stack2(i, :, :)), (i - 1) * drift_);
end

sliceViewer(permute(stack4,[2 3 1]), 'ColorMap', parula(256))
colorbar

%% save
stack = stack4;
save(fullfile(input_folder, 'image_no_drift_propio.mat'), 'stack');
