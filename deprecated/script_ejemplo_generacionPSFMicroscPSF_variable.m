% script_ejemplo_generacionPSFMicroscPSF
%
% Este script de ejemplo genera una PSF a traves de la funcion MicroscPSF y
% la propaga utilizando la integral de fresnel (con la funcion
% fresnelABCDIntegral).

clear, close all;
addpath src/psf_generation
addpath src/aux_functions
addpath src/plot

%Archivo de salida
folder = 'Output/PSF';
output_filename = 'MicroscPSF_3D';

%Parametros del microscopio y muestra
NA = 1.4;
M = 60;
m = 2;
ti0 = 120; %micrones
nm = 1.33;
lambda = 0.5; %micrones
sample_depth = 0.5; %micrones
sample_width = 1.8; %micrones

% Parametros de calculo
sizeLateral = 25;
sizeAxial = 25;

% Magnificaciones luego de propagaciones (ver a ojo)
mid_magnification = 1000;
final_magnification = 100;

%Parametros del sistema
f1 = 0.1; %metros
f2 = 0.2; %metros
f0 = 0.4; %metros
n1 = 1; %indice de refracción del medio
n2 = 1.5; %indice de refracción de la lente f0.
theta = 0.23267; %inclinacion de la lente f0.

%delta camara y plano observable
camera_delta = -0.107218; %metros
observable_plane = ceil(sizeAxial/2); %Plano observable en la mitad de la muestra.

%% Configuracion para generacion de PSF y propagacion
pZ_vector = linspace(0, sample_depth, sizeAxial);
resLateral = sample_width / sizeLateral;
resAxial = sample_depth / sizeAxial;


%Parametros para generacion de la PSF (MicroscPSF)
params.size = [sizeLateral sizeLateral sizeAxial];
params.NA = NA;
params.M = M;
params.resLateral = resLateral * 1e-6;
params.resAxial = resAxial * 1e-6;
params.sizeLateral = sizeLateral;
params.sizeAxial = sizeAxial;
params.pZ = pZ_vector * 1e-6;
params.lambda = lambda * 1e-6;

%Parametros de configuracion de matrices (excepto pZ)
paramsmatrix.f1 = f1 * 1e6;
paramsmatrix.f2 = f2 * 1e6;
paramsmatrix.f0 = f0 * 1e6;
paramsmatrix.n1 = n1;
paramsmatrix.n2 = n2;
paramsmatrix.M = M;
paramsmatrix.m = m;
paramsmatrix.nm = nm;
paramsmatrix.theta = theta;
paramsmatrix.camera_delta = camera_delta * 1e6;

[~, ~, x_start, y_start] = generateGrid([params.size(1), params.size(2)], [resLateral resLateral], true);
[~, ~, x_mid, y_mid] = generateGrid([params.size(1), params.size(2)], mid_magnification * [resLateral resLateral], true);
[~, ~, x_final, y_final] = generateGrid([params.size(1), params.size(2)], final_magnification * [resLateral resLateral], true);

%% Generacion del campo electrico
search_factor = 5;
E = generatePSF3DWithMicroscPSF(params, search_factor);

%% Propagacion por el primer sistema.
E_mid = zeros(params.sizeAxial, params.sizeAxial, params.sizeLateral, params.sizeLateral);

%Se guardan los axis en la estructura para ser pasados para la integral.
axis.x_start = x_start;
axis.y_start = y_start;
axis.x = x_mid;
axis.y = y_mid;

for index_position = 1 : sizeAxial
    for index_plane = 1 : sizeAxial
            paramsmatrix.pZ = pZ_vector(index_position);
            mat_x = generateXMatrix(1, paramsmatrix);
            mat_y = generateYMatrix(1, paramsmatrix);
            E_parcial = squeeze(E(index_position, index_plane, :, :));
            E_parcial = fresnelABCDIntegral(E_parcial, axis, lambda, mat_x, mat_y); 
            E_mid(index_position, index_plane, :, :) = E_parcial;
    end
end
%% Propagacion por el segundo sistema.
E_final = zeros(params.sizeAxial, params.sizeAxial, params.sizeLateral, params.sizeLateral);

axis.x_start = x_mid;
axis.y_start = y_mid;
axis.x = x_final;
axis.y = y_final;

mat_x = generateXMatrix(2, paramsmatrix);
mat_y = generateYMatrix(2, paramsmatrix);
for index_position = 1 : sizeAxial
    for index_plane = 1 : sizeAxial
        E_parcial = squeeze(E_mid(index_position, index_plane, :, :));
        E_parcial = fresnelABCDIntegral(E_parcial, axis, lambda, mat_x, mat_y);
        E_final(index_position, index_plane, :, :) = E_parcial;
    end
end

%% Obtengo la intensidad de la PSF
PSFvector = abs(E_final);
PSFvector = PSFvector / max(PSFvector(:));

%% Plot axial de la PSF.
% Generacion de coordenadas
[~, ~, xPixel, yPixel] = generateGrid([sizeLateral sizeLateral], [1 1], false);
zPixel = linspace(1, sizeAxial, sizeAxial);

particle_plane = observable_plane;

figure()
plotAxial(squeeze(PSFvector(particle_plane, :, :, :)), zPixel, xPixel, 'x')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

figure()
plotAxial(squeeze(PSFvector(particle_plane, :, :, :)), zPixel, yPixel, 'y')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');


%% Guardado de parametros para convolucion y de PSF.
check_folder(folder);
ext = '.mat';
add = 'PSF';

params_conv_filename = 'params_conv';
PSF_filename = 'PSF';

[folder_name, timestamp] = generate_name_with_time(output_filename, add);
folder_name = fullfile(folder, folder_name);
mkdir(folder_name);

params_conv_file = generate_name_with_time(params_conv_filename, output_filename, timestamp);
params_conv_file = fullfile(folder_name, [params_conv_file ext]);

PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
PSF_file = fullfile(folder_name, [PSF_file ext]);

params_conv.sizeLateral = sizeLateral;
params_conv.sizeAxial = sizeAxial;
params_conv.resLateral = sample_width / sizeLateral;
params_conv.resAxial = sample_depth / sizeAxial * (m * M)^2;
params_conv.pZ_vector = linspace(0, sample_depth, sizeAxial);
params_conv.observable_plane = observable_plane;

PSF = PSFvector;
sigma = zeros(sizeAxial, sizeAxial, 2);

% save(params_conv_file, 'params_conv')
% save(PSF_file, 'PSF', 'sigma')
