% script_ejemplo_generacionPSFMicroscPSF
%
% Este script de ejemplo genera una PSF a traves de la funcion MicroscPSF y
% la propaga utilizando la integral de fresnel (con la funcion
% fresnelABCDIntegral).

clear, close all;
addpath src/psf_generation
addpath src/aux_functions
addpath src/plot

%Archivo de salida
folder = 'Output/PSF';
output_filename = 'MicroscPSF_3D_oversampled_2';

%Parametros del microscopio y muestra
NA = 1.45;
M = 60 * 2;
ti0 = 120; %micrones
ni = 1.5;
ns = 1.5;
nc = 1;
lambda = 0.46; %micrones

oversamplingLateral = 1;
oversamplingAxial = 2;

% Parametros de calculo
sizeLateralMuestra = 60 * oversamplingLateral;
sizeAxialMuestra = 100 * oversamplingAxial; 

resLateral = 0.054 / oversamplingLateral; %micrones
resAxial = 0.02778 / oversamplingAxial; %micrones
pZ = 16000 / 3600 / 4 / oversamplingAxial; %micrones

%% Configuracion para generacion de PSF y propagacion



%Parametros para generacion de la PSF (MicroscPSF)
params.size = [sizeLateralMuestra sizeLateralMuestra sizeAxialMuestra];
params.NA = NA;
params.M = M;
params.resLateral = resLateral * 1e-6;
params.resAxial = resAxial * 1e-6;
params.pZ = pZ * 1e-6;
params.lambda = lambda * 1e-6;
params.ns = ns;
params.ni = ni;
params.ni0 = ni;
params.ng = nc;
params.ng0 = nc;


%% Generacion de la PSF
[~, PSF] = generatePSF3DWithMicroscPSF(params);
PSF = PSF / max(PSF(:));

%%
figure()
plotAxial(PSF, [], 'x')
sigma = sigma_estimation(PSF);


%%
figure()
plotLateral(squeeze(PSF(10, :, :)))
axis square
colorbar

%% 
figure
sliceViewer(permute(PSF, [2 3 1]),'Colormap', parula(256));
%% Guardado de parametros para convolucion y de PSF.
check_folder(folder)
ext = '.mat';
add = 'PSF';

params_conv_filename = 'params_conv';
PSF_filename = 'PSF';

[folder_name, timestamp] = generate_name_with_time(output_filename, add);
folder_name = fullfile(folder, folder_name);
mkdir(folder_name);

params_conv_file = generate_name_with_time(params_conv_filename, output_filename, timestamp);
params_conv_file = fullfile(folder_name, [params_conv_file ext]);

PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
PSF_file = fullfile(folder_name, [PSF_file ext]);

params_conv.sizeAxial = sizeAxialMuestra;
params_conv.sizeLateral = sizeLateralMuestra;
params_conv.resLateral = resLateral;
params_conv.resAxial = resAxial;

save(params_conv_file, 'params_conv')
save(PSF_file, 'PSF', 'sigma')
