
% script_ejemplo_convolucion3D
%
% Este script de ejemplo genera distintas formas para luego convucionarlas
% con una PSF 3D de entrada.

clear, close all;
clc
addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/psf_generation
addpath src/plot

%Archivo de salida
folder = './Output/convolution/tesis_demostracion';
output_filename = 'demostracion_stack'; 

sizeAxial = 200;
sizeLateral = 200;
resLateral = .5;
resAxial = 30;

% Particulas
particulas = 1000;
alpha = 100;

%Ruido y fondo
background_value = 0;
background_mode = 'constant';

snr_esperada = Inf;
noise_distribution = 'gaussian';

sigma = 5;

%[PSF_work, ~] = generateGaussianSpherePSF3D([sizeAxial sizeLateral sizeLateral], sigma);

oversize = 4;

params_psf.sample_depth = sizeAxial * resAxial * oversize;
params_psf.lambda = 0.5;
params_psf.w0 = 2 * sigma;
params_psf.sample_width = sizeLateral * resLateral * oversize;
params_psf.nm = 1;
params_psf.z = params_psf.sample_depth / 2;

[PSF_work, sigma_work] = generateGaussianPSF3DCircle([sizeAxial * oversize sizeLateral * oversize sizeLateral * oversize], params_psf);


%% Generacion de coordenadas para graficos
[~, ~, xPixel, yPixel] = generateGrid([sizeLateral sizeLateral], [1 1], false);
zPixel = linspace(1, sizeAxial, sizeAxial);

%% Plot de la PSF axial.

figure()
plotAxial(PSF_work, [], 'x')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

figure()
plotAxial(PSF_work, [], 'y')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');


%% Generacion de la entrada.
% Las funciones generate suman, es decir que es acumulativo el agregado de
% formas. 
% Devuelven las posiciones en el rango [1, sz(entrada)].
entrada = zeros(sizeAxial, sizeLateral, sizeLateral);
positions = [];

%[entrada, positions] = generateEllipsoid(entrada, positions, 10000, [100, 100, 100], [80 50, 50], alpha);
% [entrada, positions] = generateDot(entrada, positions, 1, [10 10 10], 0, 100, 'onlydot');
% [entrada, positions] = generateDot(entrada, positions, 1, [15 10 10], 0, 100, 'onlydot');
[entrada, positions] = generateDot(entrada, positions, 1, [50 50 50], 0, 100, 'onlydot');
[entrada, positions] = generateDot(entrada, positions, 1, [150 150 150], 0, 100, 'onlydot');
%[entrada, positions] = generateDot(entrada, positions, 1, [135 150 150], 0, 100, 'onlydot');
% [entrada, positions] = generateDot(entrada, positions, 1, [90 90 90], 0, 100, 'onlydot');
% [entrada, positions] = generateDot(entrada, positions, 1, [120 120 90], 0, 100, 'onlydot');

%% Plot ejes laterales con entrada y particulas
figure()
plotImageAndParticles(squeeze(sum(entrada(:, :, :),1)), positions(:, 1:2));
title(['Entrada en plano observable | ' ('size = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'px | sizePixelLat \approx ' + string(resLateral) + ' um')]);

%%
figure()
x = positions(:, 1);
y = positions(:, 2);
z = positions(:, 3);
scatter(x, y, [], z, 'filled')
xlim([1 sizeLateral])
ylim([1 sizeLateral])
caxis([1 sizeAxial])
colorbar
axis square
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);


%%
figure()
plot3d(positions, [1 sizeLateral 1 sizeLateral 1 sizeAxial], 'b.')
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

%%
%h = figure('units','normalized','outerposition',[0 0 1 1]);
s = scatter3(positions(:, 2), positions(:, 1), positions(:, 3), 'filled', 'MarkerFaceAlpha', 0.5, 'MarkerEdgeAlpha', 0.5);
axis equal
xlim([0 200])
ylim([0 200])
zlim([0 200])
xlabel('x [px]')
ylabel('y [px]')
zlabel('z [px]')
az = -56.8145;
el = 7.2;
view([az el])
cropSize = 10;
%pbaspect([2 1 1])
%guardar_figura(true, h, gca, cropSize*2, 'scatter', folder)
exportgraphics(gcf, fullfile(folder, ['scatter' '.png']), 'Resolution', 300)

%% Plot ejes axiales con entrada y particulas.
figure()
plotImageAndParticlesAxial(entrada, [], 'x', positions, 'sum')
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

figure()
plotImageAndParticlesAxial(entrada, [], 'y', positions, 'sum')
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

%% Convolucion y agregado de ruido y fondo.
salida = convn(entrada, PSF_work, "same");
%[salida, background] = addBackground(salida, background_value, background_mode);
%[salida, noise] = addNoise(salida, snr_esperada, noise_distribution);
%noise = zeros(size(salida));
%snr_salida = snr(salida, noise);
%background_salida = background(1);

%% Select roi
% [salida_roi, positions_roi] = getRoi(salida, positions, [size(salida, 2)/2 - 20, size(salida, 2)/2 + 20, size(salida, 3)/2 - 20, size(salida, 3) / 2  + 20, ceil(0.25 * 80), ceil(0.75 * 80)]);
% salida = salida_roi;
% positions = positions_roi;

%% Me quedo con la zona que me interesa
% centerAxial = ceil(size(salida, 1) / 2);
% centerLateral = ceil(size(salida, 2) / 2);
% salida = salida(centerAxial - floor(sizeAxial / 2) + 1 - mod(sizeAxial, 2) : centerAxial + floor(sizeAxial / 2), :, :);
% salida = salida(:, centerLateral - floor(sizeLateral / 2) + 1 - mod(sizeLateral, 2) : centerLateral + floor(sizeLateral / 2), :);
% salida = salida(:, :, centerLateral - floor(sizeLateral / 2) + 1 - mod(sizeLateral, 2) : centerLateral + floor(sizeLateral / 2));

%%
figure
obs = ceil(sizeAxial/2);
imagesc(squeeze(salida(obs, :, :)))
axis square
colorbar

%% Plot 4d convolucion
figure()
params_4d.xlabel = 'x [px]';
params_4d.ylabel = 'y [px]';
params_4d.zlabel = 'z [px]';
params_4d.backgroundalpha = 1;
params_4d.FontSize = 12;
plot4d(salida, params_4d)
    title(['Convolucion | sigma = ' num2str(sigma) ' px'])

clear params_4d

%% Plot lateral con salida y particulas.
figure()
plotImageAndParticlesAxial(salida, [], 'x', positions, 'sum')
tit = {['Salida convolucionada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

figure()
plotImageAndParticlesAxial(salida, [], 'y', positions, 'sum')
tit = {['Salida convolucionada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

%% Guardado (solo guarda datos del plano observable).
check_folder(folder);

save_stack_as_tiff(salida, fullfile(folder, 'stack2.tif'));