clear all, close all;
clc;
addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot

% Archivo de entrada
input_folder = 'C:/Users/Matias/Desktop/2022-02-22-Argolight_Lineas_3D/';
folder_filename = '2022-02-22_17.24.33_Argoligth-Lineas-3D_60x1.6x2xNA1.45_LedUV_2000.0028msRS';
input_file = 'MMStack_Pos0.ome.tif';

%Se genera la PSF
inputPSF_folder = 'Output/PSF';
inputPSF_folder = fullfile(inputPSF_folder, '2022-01-03_11.19.53_PSF_MicroscPSF_3D_oversampled_2');
[params, PSF_work, sigma_work] = load_psf_file(inputPSF_folder);

PSF_work = PSF_work/max(PSF_work(:));

sliceViewer(permute(PSF_work, [2 3 1]), 'ColorMap', parula(256));
colorbar
%%
% sizeAxial = params.sizeAxial;
sizeAxial = ceil(params.sizeAxial/2);
sizeLateral = ceil(params.sizeLateral/2);
resLateral = params.resLateral;
resAxial = params.resAxial;

observablePlanePSF = ceil(params.sizeAxial/2) + 1;

% Archivo de salida
folder = fullfile(input_folder, folder_filename);
output_filename = 'output.mat'; 


%%
distance = 1500;
load(fullfile(input_folder, folder_filename, input_file));

sliceViewer(permute(stack, [2 3 1]), 'Colormap', parula(256));
h_rect = imrect();
pos_rect = h_rect.getPosition();
pos_rect = round(pos_rect);
salida = stack(:, pos_rect(2) + (0:pos_rect(4)), pos_rect(1) + (0:pos_rect(3)));

%%
sliceViewer(permute(salida, [2 3 1]), 'ColorMap', parula(256));

%% Guardado (solo guarda datos del plano observable).
check_folder(folder);

ext = '.mat';

PSF_filename = 'psf';
roi_filename = 'roi';

output_filename = [output_filename '_distance' num2str(distance) 'nm'];

[folder_name, timestamp] = generate_name_with_time(output_filename, '3D_vp');
folder_name = fullfile(folder, folder_name);
mkdir(folder_name)

roi_file = generate_name_with_time(roi_filename, output_filename, timestamp);
roi_file = fullfile(folder_name, [roi_file ext]);

PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
PSF_file = fullfile(folder_name, [PSF_file ext]);



% Se selecciona como roi solo el plano observable.
roi = salida(1:90, :, :);

% Se guardan solo los datos de sigma y de la PSF en el plano observable.
PSF = PSF_work;

pxSize = params.resLateral * 1000; %se pasa a nm para SUPPOSe
pxSizeAxial = params.resAxial * 1000;

sigma = sigma_work;
save(roi_file, 'roi', 'sigma', 'pxSize', 'pxSizeAxial');
save(PSF_file, 'PSF', 'sigma');