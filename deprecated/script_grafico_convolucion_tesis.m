
% script_ejemplo_convolucion3D
%
% Este script de ejemplo genera distintas formas para luego convucionarlas
% con una PSF 3D de entrada.

clear, close all;
clc
addpath src/convolution
addpath src/psf_generation
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot

%Se genera la PSF
[PSF_work, sigma_work] = generateGaussianSpherePSF3D([1 51 51], [3 3 3]);

PSF_work = PSF_work/max(PSF_work(:));

sizeAxial = 1;
sizeLateral = 51;
resLateral = 54; %nm
resAxial = 1; %um

% Particulas
alpha = 100;

%Ruido y fondo
background_value = 0;
background_mode = 'constant';

snr_esperada = Inf;
noise_distribution = 'gaussian';

font_size = 12;

scalebarSize = 300;
figCropSize = 5;

%% Generacion de coordenadas para graficos
[~, ~, xPixel, yPixel] = generateGrid([sizeLateral sizeLateral], [1 1], false);
zPixel = linspace(1, sizeAxial, sizeAxial);

%% Plot de la PSF.
h = figure('units','normalized','outerposition',[0 0 1 1]);
imagesc([1 size(PSF_work, 3)] * resLateral, [1 size(PSF_work, 2)] * resLateral, squeeze(PSF_work))
xticks([])
yticks([])
axis tight equal
set(gca,'FontSize', font_size,'YDir','normal');
% Barra de escala.
scalebar.imageSize = [size(PSF_work, 3), size(PSF_work, 2)] * resLateral;
scalebar.size = [scalebarSize scalebarSize / 2];
scalebar.pxSize = 1;
scalebar.separation = round([size(PSF_work, 3), size(PSF_work, 2)] * 0.05) * resLateral;
scalebar.color = 'white';
scalebar.units = 'nm';
scalebar.fontSize = font_size;
scalebar.label = [];
scalebar.x = true;
scalebar.y = false;
plot_scalebar_3d(scalebar)
%colorbar('visible', 'off')
% colorbar
%caxis([0 1])
%title(['ROI plano ' num2str(j * run.pxSizeAxial) ' nm'])
pbaspect([2 1 1])
guardar_figura(true, h, gca, figCropSize*2, 'grafico_conv_psf', '.')


%% Generacion de la entrada.
% Las funciones generate suman, es decir que es acumulativo el agregado de
% formas. 
% Devuelven las posiciones en el rango [1, sz(entrada)].
entrada = zeros(sizeAxial, sizeLateral, sizeLateral);
positions = [];

[entrada, positions] = generatePimpedTwoLines(entrada, positions, 5, 1, 100, 30, 100);

%%
plot_scatter_2d_xy(positions, [resLateral resAxial], [1 51 1 51 1 1], font_size, '.', 'grafico_conv_gt', 0.5, [1 1 1 51 1 51], figCropSize, [0 0.4470 0.7410])

%% Plot de la PSF.
h = figure('units','normalized','outerposition',[0 0 1 1]);
imagesc([1 size(entrada, 3)] * resLateral, [1 size(entrada, 2)] * resLateral, squeeze(entrada))
xticks([])
yticks([])
axis tight equal
set(gca,'FontSize', font_size,'YDir','normal');
% Barra de escala.
scalebar.imageSize = [size(entrada, 3), size(entrada, 2)] * resLateral;
scalebar.size = [scalebarSize scalebarSize / 2];
scalebar.pxSize = 1;
scalebar.separation = round([size(entrada, 3), size(entrada, 2)] * 0.05) * resLateral;
scalebar.color = 'white';
scalebar.units = 'nm';
scalebar.fontSize = font_size;
scalebar.label = [];
scalebar.x = true;
scalebar.y = false;
plot_scalebar_3d(scalebar)
%colorbar('visible', 'off')
% colorbar
%caxis([0 1])
%title(['ROI plano ' num2str(j * run.pxSizeAxial) ' nm'])
pbaspect([2 1 1])
guardar_figura(true, h, gca, figCropSize*2, 'grafico_conv_entrada', '.')


%% Convolucion y agregado de ruido y fondo.
salida = convn(entrada, PSF_work, "same");

%% Plot de la PSF.
h = figure('units','normalized','outerposition',[0 0 1 1]);
imagesc([1 size(salida, 3)] * resLateral, [1 size(salida, 2)] * resLateral, squeeze(salida))
xticks([])
yticks([])
axis tight equal
set(gca,'FontSize', font_size,'YDir','normal');
% Barra de escala.
scalebar.imageSize = [size(salida, 3), size(salida, 2)] * resLateral;
scalebar.size = [scalebarSize scalebarSize / 2];
scalebar.pxSize = 1;
scalebar.separation = round([size(salida, 3), size(salida, 2)] * 0.05) * resLateral;
scalebar.color = 'white';
scalebar.units = 'nm';
scalebar.fontSize = font_size;
scalebar.label = [];
scalebar.x = true;
scalebar.y = false;
plot_scalebar_3d(scalebar)
%colorbar('visible', 'off')
% colorbar
%caxis([0 1])
%title(['ROI plano ' num2str(j * run.pxSizeAxial) ' nm'])
pbaspect([2 1 1])
guardar_figura(true, h, gca, figCropSize*2, 'grafico_conv_salida', '.')

function plot_scatter_2d_xy(pos, pxSize, zoom, font_size, images_path, name, alpha_scatter, roi, cropSize, color, r)
    h = figure('units','normalized','outerposition',[0 0 1 1]);
    zoom = zoom .* [pxSize(1) pxSize(1) pxSize(1) pxSize(1) pxSize(2) pxSize(2)];
    scatter(pos(:, 2) .* pxSize(1), pos(:, 1) .* pxSize(1), 'o', 'filled', 'MarkerFaceAlpha', alpha_scatter, 'MarkerEdgeAlpha', alpha_scatter, 'MarkerFaceColor', color, 'MarkerEdgeColor', color)
    set(gca, 'FontSize', font_size)
    hold on
    if roi ~= zoom
        plot_square(roi([3 4 1 2]) .* [pxSize(1) pxSize(1) pxSize(1) pxSize(1)])
    end
%     th = 0:pi/50:2*pi;
%     r = r * pxSize(1);
%     x = 26 * pxSize(1);
%     y = 26 * pxSize(1);
%     xunit = r * cos(th) + x;
%     yunit = r * sin(th) + y;
%     scatter(xunit, yunit, 'k')
    xlabel('x [nm]')
    ylabel('y [nm]')
    set(gca,'TickDir','out');
    axis tight equal
    grid on
    xlim([zoom(1) zoom(2)])
    ylim([zoom(3) zoom(4)])
    pbaspect([2 1 1])
    guardar_figura(true, h, gca, cropSize*2, name, images_path)
end