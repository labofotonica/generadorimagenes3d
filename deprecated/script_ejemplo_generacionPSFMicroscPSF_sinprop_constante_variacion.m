% script_ejemplo_generacionPSFMicroscPSF
%
% Este script de ejemplo genera una PSF a traves de la funcion MicroscPSF y
% la propaga utilizando la integral de fresnel (con la funcion
% fresnelABCDIntegral).

clear, close all;
clc
addpath src/psf_generation
addpath src/aux_functions
addpath src/plot

%Archivo de salida
folder = 'Output/PSF';
pre_output_filename = 'MicroscPSF_3D_simulacion_lineas_argolight';

%Parametros del microscopio y muestra
NA = 1.4;
M = 60;
ti0 = 120; %micrones
nm = 1.5;
lambda = 0.45; %micrones
sample_depth = 1; %micrones
sample_width = 1; %micrones

% Parametros de calculo
sizeLateral = 200;
sizeAxial = 200; 

lambda_ = lambda * [0.85 0.8 0.95 1 1.05 1.1 1.5];

for i = 1 : size(lambda_, 2)
        
        output_filename = [pre_output_filename '_NA' num2str(NA) '_lambda', num2str(lambda_(i)) '_nm' num2str(nm)];
        
        %% Configuracion para generacion de PSF y propagacion
        pZ = sample_depth / 2; % PSF en la mitad de l la muestra
        pxSizeLateral = sample_width / sizeLateral;
        pxSizeAxial = sample_depth / sizeAxial;


        %Parametros para generacion de la PSF (MicroscPSF)
        params.size = [sizeLateral sizeLateral sizeAxial];
        params.NA = NA;
        params.M = M;
        params.resLateral = pxSizeLateral * 1e-6;
        params.resAxial = pxSizeAxial * 1e-6;
        params.sizeLateral = sizeLateral;
        params.sizeAxial = sizeAxial;
        params.pZ = pZ * 1e-6;
        params.lambda = lambda_(i) * 1e-6;


        %% Generacion de la PSF
        [~, PSF] = generatePSF3DWithMicroscPSF(params);
        PSF = PSF / max(PSF(:));
        figure()
        plotAxial(PSF, [], 'x')
        sigma = sigma_estimation(PSF, 'fit');


        %%
        figure()
        plotLateral(squeeze(PSF(40, :, :)))
        colorbar
        %% Guardado de parametros para convolucion y de PSF.
        check_folder(folder)
        ext = '.mat';
        add = 'PSF';

        params_conv_filename = 'params_conv';
        PSF_filename = 'psf';

        [folder_name, timestamp] = generate_name_with_time(output_filename, add);
        folder_name = fullfile(folder, folder_name);
        mkdir(folder_name);

        params_conv_file = generate_name_with_time(params_conv_filename, output_filename, timestamp);
        params_conv_file = fullfile(folder_name, [params_conv_file ext]);

        PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
        PSF_file = fullfile(folder_name, [PSF_file ext]);

        params_conv.sizeLateral = sizeLateral;
        params_conv.sizeAxial = sizeAxial;
        params_conv.resLateral = sample_width / sizeLateral;
        params_conv.resAxial = sample_depth / sizeAxial;
        params_conv.NA = NA;
        params_conv.lambda = lambda_(i);

        save(params_conv_file, 'params_conv')
        save(PSF_file, 'PSF', 'sigma', 'pxSizeLateral', 'pxSizeAxial')
end