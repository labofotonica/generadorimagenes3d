

function [corrected_stack, drift] = drift_correction_max_method(stack2, start_slice, end_slice, drift_)
    if (nargin < 4)
        drift_ = [];
    end

    addpath src/convolution
    addpath src/convolution/shape_generators
    addpath src/aux_functions
    addpath src/plot

    %%
    if (isempty(drift_))
        sliceViewer(permute(stack2, [2 3 1]), 'Colormap', parula(256));
        h_rect = imrect();
        pos_rect = h_rect.getPosition();
        pos_rect = round(pos_rect);
        salida = stack2(:, pos_rect(2) + (0:pos_rect(4)), pos_rect(1) + (0:pos_rect(3)));


        %%
        a = [];
        stack3 = salida;
        for i = 1 : size(stack3,1)
            im = squeeze(stack3(i, :, :));
            [C,I] = max(im(:));
            [a0, a1] = ind2sub(size(im),I);
            a = [a; [a0, a1]];
            stack3(i, a0, a1) = C * 100;
        end

        figure
        sliceViewer(permute(stack3,[2 3 1]), 'ColorMap', parula(256))
        colorbar


        figure
        plot(a(start_slice:end_slice, 1), a(start_slice:end_slice, 2), 'r.')
        %%
        drift_ = mean(a(start_slice:end_slice-1, :) - a(start_slice+1:end_slice, :));
        drift_ = [drift_(2) drift_(1)];
    
    end
    stack4 = zeros(size(stack2));
    for i = 1 : size(stack2, 1)
        stack4(i, :, :) = imtranslate(squeeze(stack2(i, :, :)), (i - 1) * drift_);
    end

    %sliceViewer(permute(stack4,[2 3 1]), 'ColorMap', parula(256))
    %colorbar

    %% save
    corrected_stack = stack4;
    drift = drift_;
  
end