% script_ejemplo_convolucion3D
%
% Este script de ejemplo genera distintas formas para luego convucionarlas
% con una PSF 3D de entrada.

clear, close all;
addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot

%Archivo de salida
folder = 'Output/convolution';
output_filename = 'convolucion3D';

%Archivo de entrada
inputs_folder = 'Output/PSF';
input_folder = '2021-02-09_12.43.52_PSF_gaussian_3D';
[params, PSF_work, sigma_work] = load_psf_file(fullfile(inputs_folder, input_folder));

PSF_work = PSF_work/max(PSF_work(:));

sizeAxial = params.sizeAxial;
sizeLateral = params.sizeLateral;
resLateral = params.resLateral;
resAxial = params.resAxial;
planoObservable = 9; % relacionado con la posicion de la camara.

%Ruido y fondo
background_value = 100;
background_mode = 'constant';

snr_esperada = 50;
noise_distribution = 'gaussian';

%% Generacion de coordenadas para graficos
[~, ~, xPixel, yPixel] = generateGrid([sizeLateral sizeLateral], [1 1], false);
zPixel = linspace(1, sizeAxial, sizeAxial);

%% Plot de la PSF axial.
plano_particula = 9;

figure()
plotAxial(squeeze(PSF_work(plano_particula, :, :, :)), zPixel, xPixel, 'x')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

figure()
plotAxial(squeeze(PSF_work(plano_particula, :, :, :)), zPixel, yPixel, 'y')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');


%% Generacion de la entrada.
% Las funciones generate suman, es decir que es acumulativo el agregado de
% formas. 
% Devuelven las posiciones en el rango [1, sz(entrada)].
entrada = zeros(sizeAxial, sizeLateral, sizeLateral);
positions = [];

[entrada, positions] = generatePimpedTwoLines(entrada, positions, 2, 9, 100, 100);
%[entrada, posiciones] = generateLine(entrada, posiciones, 100, [14, 14, 9], [20, 20, 9], 100);
%[entrada, posiciones] = generateDot(entrada, posiciones, 1, [17 17 9], 1, 100, 'onlydot');


%% Plot axial con entrada y particulas.
figure()
plotImageAndParticlesAxial(entrada, zPixel, yPixel, 'x', positions)
title('Entrada | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

figure()
plotImageAndParticlesAxial(entrada, zPixel, yPixel, 'y', positions)
title('Entrada | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

%% Convolucion y agregado de ruido y fondo.
salida = convolvePSF3D(entrada, PSF_work);
[salida, background] = addBackground(salida, background_value, background_mode);
[salida, noise] = addNoise(salida, snr_esperada, noise_distribution);
snr_salida = snr(salida, noise);
background_salida = background(1);

%% Plot en el plano observable.
figure()
plotImageAndParticles(xPixel, yPixel, squeeze(salida(planoObservable, :, :)), positions(:, 1:2))
title('Salida en plano observable | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um');

%% Plot lateral con salida y particulas.
figure()
plotImageAndParticlesAxial(salida, zPixel, xPixel, 'x', positions)
title('Salida | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

figure()
plotImageAndParticlesAxial(salida, zPixel, yPixel, 'y', positions)
title('Salida | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

%% Guardado (solo guarda datos del plano observable).
check_folder(folder);

ext = '.mat';

PSF_filename = 'psf';
roi_filename = 'roi';

[folder_name, timestamp] = generate_name_with_time(output_filename, '3D_sp');
folder_name = fullfile(folder, folder_name);
mkdir(folder_name)

roi_file = generate_name_with_time(roi_filename, output_filename, timestamp);
roi_file = fullfile(folder_name, [roi_file ext]);

PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
PSF_file = fullfile(folder_name, [PSF_file ext]);

% Se selecciona como roi solo el plano observable.
roi = squeeze(salida(params.observable_plane, :, :));

zExacto = squeeze(positions(:, 3)).';
xExacto = squeeze(positions(:, 2)).';
yExacto = squeeze(positions(:, 1)).';

% Se guardan solo los datos de sigma y de la PSF en el plano observable.
PSF = zeros(params.sizeAxial, params.sizeLateral, params.sizeLateral);
sigma = zeros(params.sizeAxial, 2);

for i = 1 : params.sizeAxial
   PSF(i, :, :) = squeeze(PSF_work(i, params.observable_plane, :, :));
   sigma(i, :) = squeeze(sigma_work(i, params.observable_plane, :));
end


pxSize = params.resLateral * 1000; %se pasa a nm para SUPPOSe

save(roi_file, 'roi', 'positions', 'zExacto', 'xExacto', 'yExacto', 'pxSize', 'snr_salida', 'background_salida');
save(PSF_file, 'PSF', 'sigma');