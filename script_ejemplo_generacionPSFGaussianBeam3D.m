% script_ejemplo_generacionPSFGaussianBeam3D.m
%
% Este script de ejemplo genera una PSF de haz gaussiano 3d.

clear, close all;
addpath src/psf_generation
addpath src/aux_functions
addpath src/plot

%Archivo de salida
folder = 'Output/PSF/';
output_filename = 'gaussian_beam_3d';

sizeLateral = 31; %impar para no generar problemas de visualizacion.
sizeAxial = 31; %impar para no generar problemas de visualizacion.

pxSizeLateral = 0.054; %micrones
pxSizeAxial = 0.035;

%Parametros del sistema
nm = 1;
sample_width = sizeLateral * pxSizeLateral; % micrones
sample_depth = sizeAxial * pxSizeAxial; %micrones (sin propagacion)
z = sample_depth/2; %micrones
lambda = 0.5; % micrones
w0 = 0.25; %micrones

%%
params_gaussian.z = z;
params_gaussian.sample_depth = sample_depth;
params_gaussian.lambda = lambda;
params_gaussian.w0 = w0;
params_gaussian.sample_width = sample_width;
params_gaussian.nm = nm;


%% Se llama a la funcion de generacion
[PSF, sigma] = generateGaussianPSF3D([sizeAxial sizeLateral sizeLateral], params_gaussian);  

%% Ploteo en eje axial.

figure()
plotAxial(PSF, [], 'x')

figure()
plotAxial(PSF, [], 'y')

title('PSF Corte axial')
xlabel('z [px]')
ylabel('y [px]')

%% Ploteo en el eje lateral.
figure()
imagesc(squeeze(PSF(ceil(sizeAxial/2), :, :)))
axis square
colorbar
xlabel('x [px]')
ylabel('y [px]')
title('PSF corte lateral')

%% Guardado de parametros para convolucion y de PSF.
check_folder(folder)
ext = '.mat';
add = 'PSF';

params_conv_filename = 'params_conv';
PSF_filename = 'PSF';

[folder_name, timestamp] = generate_name_with_time(output_filename, add);
folder_name = fullfile(folder, folder_name);
mkdir(folder_name);

params_conv_file = generate_name_with_time(params_conv_filename, output_filename, timestamp);
params_conv_file = fullfile(folder_name, [params_conv_file ext]);

PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
PSF_file = fullfile(folder_name, [PSF_file ext]);

params_conv.sizeLateral = sizeLateral;
params_conv.sizeAxial = sizeAxial;
params_conv.resLateral = sample_width / sizeLateral;
params_conv.resAxial = sample_depth / sizeAxial;

save(params_conv_file, 'params_conv')
save(PSF_file, 'PSF', 'sigma')