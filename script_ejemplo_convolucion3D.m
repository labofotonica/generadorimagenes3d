% script_ejemplo_convolucion3D
%
% Este script de ejemplo genera distintas formas para luego convucionarlas
% con una PSF 3D de entrada.

clear, close all;
clc
addpath src/convolution
addpath src/convolution/shape_generators
addpath src/aux_functions
addpath src/plot

%Archivo de salida
folder = 'Output/convolution/2023-02-23_CrossedLines';
output_filename = 'CrossedLines_'; 

%Se genera la PSF
inputs_folder = 'Output/PSF/crossed_lines_tesis';
input_folder = '2023-02-23_12.13.14_PSF_gaussian_sphere_3D_sigma_3_3_6';
[params, PSF_work, sigma_work] = load_psf_file(fullfile(inputs_folder, input_folder));

PSF_work = PSF_work/max(PSF_work(:));

sizeAxial = params.sizeAxial;
sizeLateral = params.sizeLateral;
resLateral = params.resLateral; %um
resAxial = params.resAxial; %um

observablePlane = ceil(sizeAxial/2);
observablePlanePSF = ceil(params.sizeAxial/2) + 1;

% Particulas
alpha = 100;

%Ruido y fondo
background_value = 0;
background_mode = 'constant';

snr_esperada = Inf;
noise_distribution = 'gaussian';
%%
if(size(sigma_work, 1) > 1)
   sigma_work = sigma_work(observablePlanePSF, :);
end

%% Generacion de coordenadas para graficos
[~, ~, xPixel, yPixel] = generateGrid([sizeLateral sizeLateral], [1 1], false);
zPixel = linspace(1, sizeAxial, sizeAxial);

%% Plot de la PSF axial.

figure()
plotAxial(PSF_work, [], 'x')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');

figure()
plotAxial(PSF_work, [], 'y')
title('PSF | tam = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'x' +string(sizeAxial) + 'px | tamPixelLat \approx ' + string(resLateral) + ' um | tamPixelAxial \approx ' + string(resAxial) + ' um');


%% Generacion de la entrada.
% Las funciones generate suman, es decir que es acumulativo el agregado de
% formas. 
% Devuelven las posiciones en el rango [1, sz(entrada)].
entrada = zeros(sizeAxial, sizeLateral, sizeLateral);
positions = [];

[entrada, positions] = generatePimpedTwoLines(entrada, positions, 2, 9, 100, 100);

%% Plot ejes laterales con entrada y particulas
figure()
plotImageAndParticles(squeeze(sum(entrada(:, :, :),1)), positions(:, 1:2));
title(['Entrada en plano observable | ' ('size = ' + string(sizeLateral) + 'x' + string(sizeLateral) + 'px | sizePixelLat \approx ' + string(resLateral) + ' um')]);

%%
figure()
x = positions(:, 1);
y = positions(:, 2);
z = positions(:, 3);
scatter(x, y, [], z, 'filled')
xlim([1 sizeLateral])
ylim([1 sizeLateral])
caxis([1 sizeAxial])
colorbar
axis square
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);


%%
figure()
plot3d(positions, [1 sizeLateral 1 sizeLateral 1 sizeAxial], 'b.')
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

%% Plot ejes axiales con entrada y particulas.
figure()
plotImageAndParticlesAxial(entrada, [], 'x', positions, 'sum')
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

figure()
plotImageAndParticlesAxial(entrada, [], 'y', positions, 'sum')
tit = {['Entrada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

%% Convolucion y agregado de ruido y fondo.
salida = convn(entrada, PSF_work, "same");
[salida, background] = addBackground(salida, background_value, background_mode);
[salida, noise] = addNoise(salida, snr_esperada, noise_distribution);
%noise = zeros(size(salida));
snr_salida = snr(salida, noise);
background_salida = background(1);

%% Select roi
% [salida_roi, positions_roi] = getRoi(salida, positions, [size(salida, 2)/2 - 20, size(salida, 2)/2 + 20, size(salida, 3)/2 - 20, size(salida, 3) / 2  + 20, ceil(0.25 * 80), ceil(0.75 * 80)]);
% salida = salida_roi;
% positions = positions_roi;

%% Me quedo con la zona que me interesa
% centerAxial = ceil(size(salida, 1) / 2);
% centerLateral = ceil(size(salida, 2) / 2);
% salida = salida(centerAxial - floor(sizeAxial / 2) + 1 - mod(sizeAxial, 2) : centerAxial + floor(sizeAxial / 2), :, :);
% salida = salida(:, centerLateral - floor(sizeLateral / 2) + 1 - mod(sizeLateral, 2) : centerLateral + floor(sizeLateral / 2), :);
% salida = salida(:, :, centerLateral - floor(sizeLateral / 2) + 1 - mod(sizeLateral, 2) : centerLateral + floor(sizeLateral / 2));

%%
figure
obs = ceil(sizeAxial/2);
imagesc(squeeze(salida(obs, :, :)))
axis square
colorbar

%% Plot en el plano observable.
figure()
plotImageAndParticles(squeeze(salida(observablePlane, :, :)), positions(:, 1:2))
tit = {['Salida en plano observable | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

%% Plot 4d convolucion
figure()
params_4d.xlabel = 'x [px]';
params_4d.ylabel = 'y [px]';
params_4d.zlabel = 'z [px]';
params_4d.backgroundalpha = 1;
params_4d.FontSize = 12;
plot4d(salida, params_4d)
    title(['Convolucion | sigma = ' num2str(sigma_work) ' px'])

clear params_4d

%% Plot lateral con salida y particulas.
figure()
plotImageAndParticlesAxial(salida, [], 'x', positions, 'sum')
tit = {['Salida convolucionada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);

figure()
plotImageAndParticlesAxial(salida, [], 'y', positions, 'sum')
tit = {['Salida convolucionada | tam = ' num2str(sizeLateral) 'x' num2str(sizeLateral) 'x' num2str(sizeAxial) 'px' newline 'tamPixelLat \approx ' num2str(resLateral * 1000) ' nm | tamPixelAxial \approx ' num2str(resAxial) ' um']};
title(tit);
%% Generacion de poblacion inicial
initial_population = generateInitialPopulation(positions, 100, min(sigma_work(:)) * 3);

%% Guardado (solo guarda datos del plano observable).
check_folder(folder);

ext = '.mat';

PSF_filename = 'psf';
roi_filename = 'roi';
roiPlane_filename = 'roiPlane';
initial_population_filename = 'initial_population';

output_filename = [output_filename '_snr' num2str(snr_salida, 1) '_back' num2str(background_salida, 1)];

[folder_name, timestamp] = generate_name_with_time(output_filename, '3D_vp');
folder_name = fullfile(folder, folder_name);
mkdir(folder_name)

roi_file = generate_name_with_time(roi_filename, output_filename, timestamp);
roi_file = fullfile(folder_name, [roi_file ext]);

roiPlane_file = generate_name_with_time(roiPlane_filename, output_filename, timestamp);
roiPlane_file = fullfile(folder_name, [roiPlane_file ext]);

PSF_file = generate_name_with_time(PSF_filename, output_filename, timestamp);
PSF_file = fullfile(folder_name, [PSF_file ext]);



initial_population_file = generate_name_with_time(initial_population_filename, output_filename, timestamp);
initial_population_file = fullfile(folder_name, [initial_population_file ext]);

% Se selecciona como roi solo el plano observable.
roi = salida;

zExacto = squeeze(positions(:, 3)).';
xExacto = squeeze(positions(:, 1)).';
yExacto = squeeze(positions(:, 2)).';

% Se guardan solo los datos de sigma y de la PSF en el plano observable.
PSF = PSF_work;
roiPlane = squeeze(roi(observablePlane, :, :));

pxSize = params.resLateral * 1000; %se pasa a nm para SUPPOSe
pxSizeAxial = params.resAxial * 1000;

sigma = sigma_work;
save(roi_file, 'roi', 'positions', 'sigma', 'zExacto', 'xExacto', 'yExacto', 'alpha', 'pxSize', 'pxSizeAxial', 'snr_salida', 'background_salida', 'roiPlane');
save(PSF_file, 'PSF', 'sigma');
save(initial_population_file, 'initial_population');